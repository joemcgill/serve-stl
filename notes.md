## Notes:

1. √	Remove the requirement for entering a mailing address for the volunteer since it is never used.  
	These fields will no longer be displayed on the website.
	The address information is on the Liability Waiver that each volunteer signs.  This will continue to allow liability protection for the host churches.

2.	Allow a volunteer to have the ability to sign up for a project on Saturday and another on Sunday.
	The programmer will review to determine feasibility.

3.	Allow the volunteer to have the ability to communicate with the project leader.
	The programmer will add a link to open the email of the project leader..

4. √ 	Replace e-mail address as the “user Name”  with a volunteer supplied user name.
	The programmer will add a User Name Field on the website.

	The email address will continue to be required.  A phrase will be added to indicate the the email will only be used to communicate with the project leader.

	Should the volunteer not have an email address the project leader will use the email address “noemail@servestlouis.org”  This will alert the project leader to make 		 contact with the volunteer by phone.

	You will need to communicate this information to the host churches after the program changes have been implemented and you have  been notified (mid to late July).

	There remains one problem after the program changes have been implemented to require a volunteer provided user name.
	The programmer will attempt to use the first name and last name of the current 64 users on the website to convert them to a user name.

	I will send an email to each of the users telling them of this change and asking them to assign a valid username to the account.

Thank you for your participation on the call yesterday.  I hope these changes will simplify your job in signing up volunteers and entering projects.  Please let me know if we missed any of your suggestions.


## MYSQL

Add a user_name field:

```
alter table users add user_name varchar(64) after host_id;
```

Update the users that are already in the system with user_names:

```
update users set user_name = lower(concat(first_name, last_name)) where user_name is null;
```
