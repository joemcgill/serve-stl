<?php

class Subscriber extends AppModel {
  public $validate = array(
      'email' => array(
          'required' => array(
            'rule' => array('notEmpty'),
            'message' => 'An email is required'
          ),
          'isUnique' => array(
            'rule' => 'isUnique',
            'message' => 'Email already in use'
          ),
          'email' => array(
            'rule' => 'email',
            'message' => 'Please enter a valid email'
          )
      )
  );  
}

?>