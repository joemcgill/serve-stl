<?php

class Church extends AppModel {
  public $hasMany = 'Project';
  
  public $validate = array(
    'phone' => array('rule' => 'notEmpty', 'required' => true)
  );
}

?>