<?php

class Page extends AppModel {
  public $validate = array(
    'title' => array('rule' => 'notEmpty', 'required' => true),
    'content' => array('rule' => 'notEmpty', 'required' => true),    
    'slug' => array('rule' => 'notEmpty', 'required' => true)
  );
  
  public $hasMany = array(
    'children' => array(
       'className' => 'Page',
       'foreignKey' => 'parent_id',
       'order' => 'order ASC',
    )
  );

  public $belongsTo = array(
    'parent' => array(
       'className' => 'Page',
       'foreignKey' => 'parent_id'
    )
  );  
}

?>