<?php

App::uses('AuthComponent', 'Controller/Component');
class User extends AppModel {
    public $hasOne = array('Host');
    public $belongsTo = array('Home' => array(
      'className' => 'Host',
      'foreignKey' => 'host_id'
    ));
    public $hasMany = array('Project', "VolunteerSpot");

    var $displayField = 'name';
    var $virtualFields = array(
        'name' => "CONCAT(first_name, ' ', last_name)"
    );

    public $validate = array(

        'email' => array(
            'required' => array(
              'rule' => array('notEmpty'),
              'message' => 'An email is required'
            ),
            'isUnique' => array(
              'rule' => 'isUnique',
              'message' => 'Email already in use'
            ),
            'email' => array(
              'rule' => 'email',
              'message' => 'Please enter a valid email'
            )
        ),
        'password' => array(
            'required' => array(
              'rule' => array('notEmpty'),
              'message' => 'A password is required'
            ),
            'minLength' => array(
              'rule' => array('minLength', '8'),
              'message' => 'Password must be a minimum of 8 characters'
            ),
            'passwordequal' => array('rule' => 'checkpasswords', 'message' => 'Passwords don\'t match')
        ),
        'user_name' => array(
          'required' => array(
            'rule' => array('notEmpty'),
            'message' => 'A user name is required'
          ),
          'isUnique' => array(
            'rule' => 'isUnique',
            'message' => 'That user name is already in use.'
          ),
        ),
        'first_name' => array(
            'required' => array(
              'rule' => array('notEmpty'),
              'message' => 'A first name is required'
            )
        ),
        'last_name' => array(
            'required' => array(
              'rule' => array('notEmpty'),
              'message' => 'A last name is required'
            )
        ),
        'phone_number' => array(
            'required' => array(
              'rule' => array('notEmpty'),
              'message' => 'A phone number is required'
            )
        ),
        'role' => array(
            'valid' => array(
                'rule' => array('inList', array('admin', 'project_leader', 'volunteer')),
                'message' => 'Please enter a valid role',
                'allowEmpty' => false
            )
        )
    );

    function checkpasswords() {
      return (strcmp($this->data[$this->alias]['password'], $this->data[$this->alias]['confirm_password']) == 0);
    }

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }
}

?>
