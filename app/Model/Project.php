<?php

class Project extends AppModel {
  public $belongsTo = array('Host', 'User');
  public $hasMany = array(
    'Volunteers' => array(
      'className' => 'VolunteerSpot'
     )
  );
  
  public $validate   = array(
    'title'          => array('rule' => 'notEmpty', 'required' => true),
    'description'    => array('rule' => 'notEmpty', 'required' => true)
  );
}

?>
