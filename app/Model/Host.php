<?php

class Host extends AppModel {
  var $displayField = 'name';
  
  public $hasMany = array(
    'Project' => array(
      'order' => 'Project.title ASC'
    ),
    'User' => array(
      'order' => 'User.last_name ASC, User.first_name ASC'
    ),
    'children' => array(
      'order' => 'children.name ASC',
       'className' => 'Host',
       'foreignKey' => 'parent_id',
    )
  );  

  public $belongsTo = array(
    'liaison' => array(
       'className' => 'User',
       'foreignKey' => 'user_id'
    ),
    'parent' => array(
       'className' => 'Host',
       'foreignKey' => 'parent_id'
    )
  );   
  
  public $validate = array(
    'name' => array('rule' => 'notEmpty', 'required' => true),
    'contact_name' => array('rule' => 'notEmpty', 'required' => true),
    'contact_email' => array(
        'required' => array(
          'rule' => array('notEmpty'),
          'message' => 'An email is required'
        ),
        'email' => array(
          'rule' => 'email',
          'message' => 'Please enter a valid email'
        )
    ),
    'contact_phone_number' => array('rule' => 'notEmpty', 'required' => true),
    'website_url' => array('rule' => 'notEmpty', 'required' => true),    
  );
}

?>