<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
  public $components = array(
    'Session',
    'Auth' => array(
        'loginRedirect' => array('controller' => 'users', 'action' => 'dashboard'),
        'logoutRedirect' => array('controller' => 'pages', 'action' => 'display', 'home'),
        'authorize' => array('Controller'),
        'authenticate' => array(
          'Form' => array(
            'fields' => array('username' => 'user_name')
          )
        )
    )
  );

  public function beforeFilter() {
    $this->Auth->allow();
  }

  public function beforeRender() {
    $this->loadModel('Page');

    // Set up default conditions.
    $conditions = array(
      'Page.parent_id' => 0,
    );

    $pages = $this->Page->find('all', array(
      'conditions' => $conditions,
      'order' => array('Page.order' => 'asc')
    ));
    $this->set('pages', $pages);

    // TODO: will this scale???
    $this->loadModel('VolunteerSpot');
    $sum = $this->VolunteerSpot->find('all', array(
        'fields' => array('sum(VolunteerSpot.total_volunteers) as total_sum')
      )
    );
    $sum = $sum[0][0]["total_sum"];
    if ($sum === null) {
      $sum = 0;
    }
    $this->set('totalVolunteerCount', $sum);
  }

  public function forceSSL() {
    return;
    // return $this->redirect('https://www.servestlouis.org' . $this->here);
  }

  public function isAuthorized() {
    return true;
  }

  protected function user_order_array() {
    return array('last_name' => 'asc', 'first_name' => 'asc');
  }

  protected function host_order_array() {
    return array('Host.name' => 'asc');
  }

  protected function volunteerSpotForUser($user_id = null) {
    if ($user_id !== null) {
      $this->loadModel('VolunteerSpot');
      $volunteerSpots = $this->VolunteerSpot->find('all', array(
        'conditions' => array('VolunteerSpot.user_id' => $user_id)
      ));
      if (count($volunteerSpots) > 0) {
        return $volunteerSpots;
      }
    }

    return null;
  }

  protected function volunteerSpotsForProject($project_id = null) {
    if ($project_id !== null) {
      $this->loadModel('VolunteerSpot');
      return $this->VolunteerSpot->find('all', array(
        'conditions' => array('VolunteerSpot.project_id' => $project_id)
      ));
    }

    return null;
  }

  protected function projectsLeadByUser($user_id = null) {
    if ($user_id !== null) {
      $this->loadModel('Project');
      return $this->Project->find('all', array(
        'conditions' => array('Project.user_id' => $user_id)
      ));
    }

    return array();
  }

  protected function updateTotalVolunteerCountForProject($id = null) {
    if (empty($id)) {
      return false;
    } else {
      $this->loadModel('VolunteerSpot');
      $this->loadModel('Project');

      $sum = $this->VolunteerSpot->find('all', array(
          'conditions' => array('VolunteerSpot.project_id' => $id),
          'fields' => array('sum(VolunteerSpot.total_volunteers) as total_sum')
        )
      );
      $sum = $sum[0][0]["total_sum"];
      $sum = intval($sum);

      $this->Project->id = $id;
      return $this->Project->saveField('volunteer_count', $sum);
    }
  }

  protected function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, strlen($characters) - 1)];
      }
      return $randomString;
  }
}
