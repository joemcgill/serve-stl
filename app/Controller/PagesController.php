<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Pages';

  public function beforeFilter() {
    $this->Auth->allow('show', 'display');
  }
  
  public function isAuthorized() {
    return $this->Auth->user('role') === 'admin';
  }
  
  public function manage() {
    if ($this->request->is('post')) {
      $data = array();
      $data['Page']['id'] = $this->request->data['id'];
      $data['Page']['order'] = $this->request->data['order'];
      if (!$this->Page->save($data, false)) {
        throw new BadRequestException();
      }
    }
  }

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			$this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$this->render(implode('/', $path));
	}
  
  public function show($slug = null) {
    if (!$slug) {
      throw new NotFoundException(__('Page not found'));
    }

    $page = $this->Page->findBySlug($slug);
    if (!$page || !$page['Page']['is_active']) {
      throw new NotFoundException(__('Page not found'));        
    }

    $this->set('page', $page);
  }
  
  public function edit($id = null) {
    $o = $this->Page;
    if ($this->request->is('post') || $this->request->is('put')) {
      if (!$id && empty($this->data)) {
          $this->Session->setFlash(__('Invalid page.'));
          $this->redirect(array('action'=>'index'));
      }
    
      if (!$id) {
        $o->create();
      } else {
        $o->id = $id;
      }
    
      if ($o->save($this->request->data)) {
        $this->Session->setFlash(__('Update succesful!'));
        $this->redirect(array('action' => 'manage'));
      } else {
        $this->Session->setFlash(__('Unable to save changes.'));
      }      
    }
    
    if (!$this->request->data) {
      $page = ($id) ? $o->findById($id) : null;
      $this->request->data = $page;
    }
  }
  
  public function delete($id) {
    if ($this->request->is('get')) {
      throw new MethodNotAllowedException();
    }
    
    if ($this->Page->delete($id)) {
      $this->Session->setFlash(__('Page deleted!'));
      $this->redirect(array('action' => 'manage'));
    }
  }    
}
