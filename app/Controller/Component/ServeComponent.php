<?php

App::uses('Component', 'Controller');
class ServeComponent extends Component {
  var $controller = true;

  function startup(&$controller) {
  }  
  
  public function additionalVolunteersForSpot($volunteerSpot = null) {
    if ($volunteerSpot === null) {
      return array();
    } else {
      $nameDelimiter = "*|*";
      $names = explode("\n", $volunteerSpot["VolunteerSpot"]['family_info']);
      for ($i = 0; $i < count($names); $i++) {
        $names[$i] = str_replace($nameDelimiter, " ", $names[$i]);
      }
      array_pop($names);
      return $names;
    }
  }  
}
?>