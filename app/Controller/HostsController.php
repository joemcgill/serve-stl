<?php

class HostsController extends AppController {
  public $helpers = array('Html', 'Form', 'Session');
  
  function getHostOrException($id = null) {
    if (!$id) {
      throw new NotFoundException(__('Invalid host'));
    }

    $host = $this->Host->findById($id);
    if (!$host) {
      throw new NotFoundException(__('Invalid host'));        
    }
    return $host;
  }
  
  public function beforeFilter() {
    $this->Auth->allow('show');
    $this->loadModel('User');
  }
  
  public function isAuthorized() {
    return $this->Auth->user('role') !== 'volunteer';
  }
  
  public function manage() {
    $this->set('hosts', $this->Host->find('all', array(
      'order' => array('Host.name' => 'asc')
    )));
  }
  
  public function show($id = null) {
    $this->set('host', $this->getHostOrException($id));
  }
  
  public function projects($id = null) {
    $this->set('host', $this->getHostOrException($id));
  }  
  
  public function members($id = null) {
    $this->set('host', $this->getHostOrException($id));
  }
  
  public function liaisons($id = null) {
    $host = $this->getHostOrException($id);
    $this->set('host', $host);
    $this->set('children', $this->Host->find('all', array(
      'conditions' => array('Host.parent_id' => $id),
      'order' => $this->host_order_array()
    )));
  }  
  
  public function edit($id = null) {    
    if ($this->request->is('post') || $this->request->is('put')) {
      if (!$id && empty($this->data)) {
          $this->Session->setFlash(__('Invalid host.'));
          $this->redirect(array('action' => 'manage'));
      }
    
      if (!$id) {
        $this->Host->create();
      } else {
        $this->Host->id = $id;
      }
      
      $data = $this->request->data['Host'];
      $file_exists = isset($data['image']);
      if ($file_exists) {
        $file = $data['image'];
        $filename = $this->generateRandomString(6) . $file['name'];
        if ($file['error'] === UPLOAD_ERR_OK) {
          $upload_path = APP.'webroot'.DS.'files'.DS.$filename;
          if (move_uploaded_file($file['tmp_name'], $upload_path)) {
            $this->request->data['Host']['image_path'] = 'files/'.$filename;
          } else {
            $error = true;
          }
        }
      }
    
      if ($this->Host->save($this->request->data)) {
        $this->Session->setFlash(__('Update succesful!'));
        $this->redirect(array('action' => 'manage'));
      } else {
        $this->Session->setFlash(__('Unable to save changes.'));
      }      
    }
    
    if (!$this->request->data) {
      $host = ($id) ? $this->Host->findById($id) : null;
      $this->request->data = $host;
    }
    
    $this->set('parents', $this->Host->find('list', array(
      'conditions' => array('Host.is_anchor' => 1),
      'order' => $this->host_order_array()
    )));
    
    $conditions = array('not' => array('User.role' => 'volunteer'));
    if ($id !== null) {
      // $conditions["User.host_id"] = $id;
    }
    $this->set('users', $this->User->find('list', array(
      'conditions' => $conditions,
      'order' => $this->user_order_array()
    )));
  }
  
  public function delete($id) {
    if ($this->request->is('get')) {
      throw new MethodNotAllowedException();
    }
    
    if ($this->Host->delete($id)) {
      $this->Session->setFlash(__('Host deleted!'));
      $this->redirect(array('action' => 'manage'));
    }
  }
}

?>