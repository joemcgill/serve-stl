<?php

function hostFromHostsById($hosts = array(), $id = -1) {
  foreach ($hosts as $host) {
    if ($host["Host"]["id"] == $id) {
      return $host;
    }
  }

  return null;
}

class geocoder {
    static private $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=";

    static public function getLocation($address) {
        $address_encoded = urlencode($address);
        $url = self::$url.urlencode($address_encoded);

        $resp_json = self::curl_file_get_contents($url);
        $resp = json_decode($resp_json, true);

        if ($resp['status'] == 'OK') {
            return $resp['results'][0]['geometry']['location'];
        } else {
            return false;
        }
    }


    static private function curl_file_get_contents($URL){
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
            else return FALSE;
    }
}

class ProjectsController extends AppController {
  public $helpers = array('Html', 'Form', 'Session', 'Time', 'SS');

  public function beforeFilter() {
    if (Configure::read("debug") === 0) {
      if (($this->action === "show") && !$this->request->is('ssl')) {
        $this->forceSSL();
      }
    }

    $this->Auth->allow('show', 'search');
  }

  public function isAuthorized() {
    return $this->Auth->user('role') !== 'volunteer';
  }

  public function manage() {
    $this->set('projects', $this->Project->find('all', array(
      'order' => array('title' => 'asc'),
      'group' => 'Project.id'
    )));
  }

  function userLine($actor, $host) {
    $projectTitle = "N/A";
    if (isset($actor["Project"]) && isset($actor["Project"]["title"])) {
      $projectTitle = $actor["Project"]["title"];
    }
    return $actor["User"]["first_name"]
              . "\t" . $actor["User"]["last_name"]
              . "\t" . $actor["User"]["email"]
              . "\t" . $actor["User"]["address_line_1"]
              . "\t" . $actor["User"]["address_line_2"]
              . "\t" . $actor["User"]["city"]
              . "\t" . $actor["User"]["state"]
              . "\t" . $actor["User"]["zipcode"]
              . "\t" . $actor["User"]["phone_number"]
              . "\t" . $projectTitle
              . "\t" . $host["Host"]["name"];
  }

  public function export($id = null) {
    $this->Serve = $this->Components->load('Serve');

    $adminExport = $id === null;
    $options = array();
    if ($id !== null) {
      $options["conditions"] = array("VolunteerSpot.project_id" => $id);
    }

    $this->loadModel('Host');
    $hosts = $this->Host->find('all', array());

    $body = "First Name\tLast Name\tEmail\tAddress Line1\tAddress Line2\tCity\tState\tZipcode\tPhone Number\tProject\tProject Host";

    if ($adminExport) {
      $body = $body . "\n" . "Admins/Laisons/Project Leaders";

      // Get the contributor data and show it
      $this->loadModel('User');
      $contributors = $this->User->find('all', array(
          'conditions' => array('not' => array('role' => 'volunteer')),
          'order' => array('last_name' => 'asc', 'first_name' => 'asc'),
          'group' => 'User.id'
      ));
      foreach ($contributors as $contributor) {
        $host = hostFromHostsById($hosts, $contributor["User"]["host_id"]);
        $body = $body . "\n" . $this->userLine($contributor, $host);
      }
    }

    $body = $body . "\n\n" . "Volunteers (by project)";

    $this->loadModel('VolunteerSpot');
    $volunteers = $this->VolunteerSpot->find('all', $options);
    foreach ($volunteers as $volunteer) {
      $host = hostFromHostsById($hosts, $volunteer["Project"]["host_id"]);

      $body = $body . "\n\n" . $this->userLine($volunteer, $host);

      $family_members = $this->Serve->additionalVolunteersForSpot($volunteer);
      foreach ($family_members as $family_member) {
        $body = $body . "\n" . $family_member;
      }
    }

    $this->response->body($body);
    $this->response->type('tsv');

    $this->response->download('servestl_export.txt');
    return $this->response;
  }

  public function show($id = null) {
    if ($id === null) {
      throw new NotFoundException(__('Invalid project'));
    }

    $project = $this->Project->findById($id);
    if (!$project) {
      throw new NotFoundException(__('Invalid project'));
    }

    $this->set('project', $project);

    $this->loadModel('VolunteerSpot');
    $user_id = $this->Auth->user("id");
    $this->set('volunteerSpot', $this->volunteerSpotForUser($user_id));

    $canViewVolunteers = $this->userCanModify($id);
    $this->set("canViewVolunteers", $canViewVolunteers);
    if ($canViewVolunteers) {
      // TODO: load volunteer info so we can display it
      $this->set("volunteerSpots", $this->volunteerSpotsForProject($id));
    }

    if ($this->request->is('post') || $this->request->is('put')) {
      if ($this->Auth->user() === null) {
        throw new ForbiddenException();
        return;
      }

      $total_volunteers = 0;
      $this->request->data["VolunteerSpot"]["family_info"] = $this->getFamilyInfoAndCount($this->request->data, $total_volunteers);

      // ensure that the total # of volunteers doesn't exceed the number needed
      if ($total_volunteers + $project["Project"]["volunteer_count"] > $project["Project"]["number_of_volunteers_needed"]) {
        $this->Session->setFlash(__("You have too many volunteers to sign up for this project! You may need to identify a new project to volunteer for."));
        return;
      }

      $this->request->data["VolunteerSpot"]["total_volunteers"] = $total_volunteers;

      $this->request->data["VolunteerSpot"]["project_id"] = $id;
      $this->request->data["VolunteerSpot"]["user_id"] = $this->Auth->user("id");

      $signedUp = $this->VolunteerSpot->save($this->request->data);
      if ($signedUp) {
        // update the volunteer total
        $this->updateTotalVolunteerCountForProject($id);

        $view = new View($this);
        $html = $view->loadHelper('Html');
        App::uses('CakeEmail', 'Network/Email');
        $email = new CakeEmail("default");
        $email->to($project["User"]["email"]);
        $email->subject('New sign ups!');
        try {
          $email->send($this->Auth->user("name").' (and others!) has signed up to volunteer for "'.$project["Project"]["title"].'." Visit the  '.$html->link("project page", array('action' => 'show', 'full_base' => true, $id)).' for all the details (NOTE: you will need to login to view volunteer information)!');
        } catch ( Exception $e ) {
          $this->Session->setFlash( $e->getMessage() );
        }
        $this->redirect(array('action' => 'confirmation'));
      } else {
        $this->Session->setFlash(__("Failed to sign up. Make sure the project isn't full and ensure you haven't already signed up for a project."));
      }
    }
  }

  public function confirmation() {

  }

  public function delete_volunteer($id = null) {
    if ($this->request->is('get') || ($id === null)) {
      throw new MethodNotAllowedException();
    }

    $this->loadModel('VolunteerSpot');
    $volunteer = $this->VolunteerSpot->findById($id);
    $project_id = $volunteer["VolunteerSpot"]["project_id"];

    $this->checkAuthorizedToModify($project_id);

    if ($this->VolunteerSpot->delete($id)) {
      $this->updateTotalVolunteerCountForProject($project_id);

      $this->Session->setFlash(__('Volunteer Spot deleted!'));
      $this->redirect(array('action' => 'show', $project_id));
    }
  }

  public function edit($id = null) {
    if ($id !== null) {
      $this->checkAuthorizedToModify($id);
    }

    if ($this->request->is('post') || $this->request->is('put')) {
      if (!$id && empty($this->data)) {
          $this->Session->setFlash(__('Invalid project.'));
          $this->redirect(array('action' => 'manage'));
      }

      if (!$id) {
        $this->Project->create();
      } else {
        $this->Project->id = $id;
      }

      $error = false;
      $project_data = $this->request->data['Project'];
      $file_exists = isset($project_data['image']);
      if ($file_exists) {
        $file = $project_data['image'];
        $filename = $this->generateRandomString(6) . $file['name'];
        if ($file['error'] === UPLOAD_ERR_OK) {
          $upload_path = APP.'webroot'.DS.'files'.DS.$filename;
          if (move_uploaded_file($file['tmp_name'], $upload_path)) {
            $this->request->data['Project']['image_path'] = 'files/'.$filename;
          } else {
            $error = true;
          }
        }
      }

      if ($this->request->data['Project']['address_line_1'] != '') {
        $proj = $this->request->data['Project'];
        $address = $proj['address_line_1'] . ", " . $proj['address_line_2'] . ", " . $proj['city'] . ", " . $proj['state'] . " " . $proj['zipcode'];
        $loc = geocoder::getLocation($address);
        $this->request->data['Project']['latitude'] = $loc['lat'];
        $this->request->data['Project']['longitude'] = $loc['lng'];
      }

      if ($this->Project->save($this->request->data)) {

        $this->Session->setFlash(__('Update succesful!'));
        $this->redirectBasedOnRole();
      } else {
        $this->Session->setFlash(__('Unable to save changes.'));
      }
    }

    if (!$this->request->data) {
      $project = ($id) ? $this->Project->findById($id) : null;
      $this->request->data = $project;
    }

    $this->loadModel('Host');
    $this->loadModel('User');
    $this->set('hosts', $this->Host->find('list', array(
      'order' => $this->host_order_array()
    )));
    $this->set('users', $this->User->find('list', array(
      'conditions' => array('not' => array('User.role' => 'volunteer')),
      'order' => $this->user_order_array()
    )));
  }

  public function delete($id) {
    if ($this->request->is('get')) {
      throw new MethodNotAllowedException();
    }

    $this->checkAuthorizedToModify($id);

    // let's delete all of the volunteer spots under the project
    $this->loadModel('VolunteerSpot');
    $this->VolunteerSpot->deleteAll(array('VolunteerSpot.project_id' => $id), false);

    if ($this->Project->delete($id)) {
      $this->Session->setFlash(__('Project deleted!'));
      $this->redirectBasedOnRole();
    }
  }

  public function search() {
    $this->request->data = $this->request->query;

    $conditions = array();
    $this->filter("family_friendly", $conditions);
    $this->filter("skilled", $conditions);
    $this->filter("physical_labor", $conditions);
    $this->filter("indoor", $conditions);
    $this->filter("outdoor", $conditions);

    // filter by age if applicable
    $age = $this->request->data("minimum_volunteer_age");
    if (!empty($age)) {
      $conditions["Project.minimum_volunteer_age"] = $area;
    }

    // filter by keyword if applicable
    $keyword = $this->request->data("keyword");
    if (!empty($keyword)) {
      $conditions["or"][] = array('Project.title LIKE' => "%$keyword%");
      $conditions["or"][] = array('Project.description LIKE' => "%$keyword%");
      $conditions["or"][] = array('Host.name LIKE' => "%$keyword%");
    }

    // filter by date if applicable
    // TODO: make this more generic so we don't have to update it each year...
    $date = $this->request->data("date");
    if (!empty($date)) {
      if ($date == "1") {
        $start = "2017-10-07";
        $end = "2017-10-08";
      } else {
        $start = "2017-10-08";
        $end = "2017-10-09";
      }
      $conditions["Project.date >="] = $start;
      $conditions["Project.date <="] = $end;
    }

    // filter by area if applicable
    $area = $this->request->data("area");
    if (!empty($area)) {
      $conditions["Project.area"] = $area;
    }

    // filter by host if applicable
    $host_id = $this->request->data("host");
    if (!empty($host_id)) {
      $conditions["Project.host_id"] = $host_id;
    }

    $this->paginate = array(
      'limit' => 18,
      'conditions' => $conditions,
      'order' => "if(Project.volunteer_count < Project.number_of_volunteers_needed, Project.title, \"zzzzz\") ASC, Project.title ASC"
    );

    $this->loadModel('Host');
    $this->set('projects', $this->paginate('Project'));
    $this->set('hosts', $this->Host->find('list', array(
      'order' => $this->host_order_array()
    )));
  }

  function filter($field, &$conditions) {
    $value = $this->request->data($field);
    if (!empty($value)) {
      $conditions['Project.'.$field] = true;
    }
  }

  function get_url_param($param) {
    if (isset($this->params['url'][$param])) {
      return $this->params['url'][$param];
    } else {
      return null;
    }
  }

  function checkAuthorizedToModify($projectId = null) {
    if (!$this->userCanModify($projectId)) {
      throw new ForbiddenException();
    }
  }

  function userCanModify($projectId = null) {
    if ($projectId === null) {
      return false;
    }

    $loggedInUser = $this->Auth->user();
    if ($loggedInUser === null) {
      return false;
    }

    if ($loggedInUser['role'] === 'admin') {
      return true;
    }

    $project = $this->Project->findById($projectId);
    if ($project['User']['id'] === $loggedInUser['id']) {
      return true;
    }

    $this->loadModel('Host');
    $host = $this->Host->findById($project['Host']['id']);
    if ($host['liaison']['id'] === $loggedInUser['id']) {
      return true;
    }

    return false;
  }

  function redirectBasedOnRole() {
    if ($this->Auth->user('role') === 'admin') {
      $this->redirect(array('action' => 'manage'));
    } else {
      $this->redirect(array('controller' => 'hosts', 'action' => 'projects', $this->request->data['Project']['host_id']));
    }
  }

  function getFamilyInfoAndCount($requestData, &$count) {
    $count = 1;
    $info = "";

    $firstNamePrefix = "firstName_";
    $lastNamePrefix = "lastName_";
    $delimiter = "*|*";

    $i = 1;
    while (true) {
      $firstNameKey = $firstNamePrefix.$i;
      $lastNameKey = $lastNamePrefix.$i;
      if (!isset($requestData[$firstNameKey]) || !isset($requestData[$lastNameKey])) {
        break;
      } else {
        $firstName = trim($requestData[$firstNameKey]);
        $lastName = trim($requestData[$lastNameKey]);
        if (strlen($firstName) > 0) {
          $count++;
          // TODO: comment out the next line and uncomment the one below to persist addresses!
          // $info = $info.$firstName.$delimiter.$lastName."\n";
           $info = $info.$firstName.$delimiter.$lastName."\n";
        }
      }
      $i++;
    }

    return $info;
  }

  function beginsWith( $str, $sub ) {
      return ( substr( $str, 0, strlen( $sub ) ) == $sub );
  }
}

?>
