<?php

class UsersController extends AppController {

  public function beforeFilter() {
    if (Configure::read("debug") === 0) {
      if (!$this->request->is('ssl')) {
        $this->forceSSL();
      }
    }

    $this->Auth->allow('signup', 'login', 'logout', 'reset_password'); // Letting users register/logout/reset password themselves
  }

  private function setHosts() {
    $this->loadModel('Host');
    $this->set('hosts', $this->Host->find('list', array(
      'order' => array('name' => 'asc')
    )));
  }

  public function signup() {
      if ($this->request->is('post')) {
          $this->User->create();
          $this->request->data['User']['role'] = isset( $this->request->data['role'] ) ? 'project_leader' : 'volunteer';
          if ($this->User->save($this->request->data)) {
              $this->Auth->login();
              if (isset($this->params['url']['redirect_to_project'])) {
                $project_id = $this->params['url']['redirect_to_project'];
                $this->redirect(array('controller' => 'projects', 'action' => 'show', $project_id));
              } else {
                $this->redirect($this->Auth->redirect());
              }
          } else {
              $this->Session->setFlash(__('Oops, you\'re not done yet! Please correct the errors noted below and try again.'));
          }
      }

      $this->setHosts();
  }

  public function login() {
      if ($this->request->is('post')) {
          if ($this->Auth->login()) {
              if (isset($this->params['url']['redirect_to_project'])) {
                $project_id = $this->params['url']['redirect_to_project'];
                $this->redirect(array('controller' => 'projects', 'action' => 'show', $project_id));
              } else {
                $this->redirect($this->Auth->redirect());
              }
          } else {
              $this->Session->setFlash(__('Invalid user name/password combination, try again'));
          }
      }
  }

  public function logout() {
      $this->redirect($this->Auth->logout());
  }

  public function manage() {
    $this->set('backend_users', $this->User->find('all',
      array(
        'conditions' => array('not' => array('role' => 'volunteer')),
        'order' => array('last_name' => 'asc', 'first_name' => 'asc'),
        'group' => 'User.id'
      )
    ));

    $this->set('volunteers', $this->User->find('all',
      array(
        'conditions' => array('role' => 'volunteer'),
        'order' => array('last_name' => 'asc', 'first_name' => 'asc'),
        'group' => 'User.id'
      )
    ));

    $this->loadModel('VolunteerSpot');
    $sum = $this->VolunteerSpot->find('all', array(
        'fields' => array('sum(VolunteerSpot.total_volunteers) as total_sum')
      )
    );
    $sum = $sum[0][0]["total_sum"];
    $this->set('volunteerCount', $sum);
  }

  public function dashboard() {
    $this->set("volunteerSpot", $this->volunteerSpotForUser($this->Auth->user("id")));
    $this->set("projects", $this->ProjectsLeadByUser($this->Auth->user("id")));
  }

  public function reset_password() {
    if ($this->request->is('post') || $this->request->is('put')) {
      $user = $this->User->findByEmail($this->request->data["User"]["email"]);
      if (!$user) {
        $this->Session->setFlash(__('Email not found. Did you enter it correctly?'));
        return;
      }

      $new_password = $this->generateRandomString(10);
      $this->User->id = $user["User"]["id"];
      $saved = $this->User->saveField("password", $new_password);
      if (!$saved) {
        $this->Session->setFlash(__('There was a problem when reseting your password. Please try again.'));
        return;

      }

      // send reset email
      App::uses('CakeEmail', 'Network/Email');
      $email = new CakeEmail("default");
      $email->to($user["User"]["email"]);
      $email->subject('Your new password');
      $email->send("Your new password for the Serve St. Louis website is:<br />".$new_password);

      $this->Session->setFlash(__('Your new temporary password should arrive in your inbox shortly.'));
    }
  }

  public function update_password($id = null) {
    $loggedInUser = $this->Auth->user();
    $loggedInUserId = $loggedInUser['id'];
    if ($id !== $loggedInUserId && $id !== null) {
      // if it's a user other than the actual user updating their password, make sure sure it's an admin
      if ($loggedInUser['role'] !== 'admin') {
        $this->Session->setFlash(__('Not authorized to change password!'));
        $this->redirect(array('action' => 'dashboard'));
        return;
      }
    } else {
      $id = $loggedInUserId;
    }

    if ($this->request->is('post') || $this->request->is('put')) {
      // confirm password matches and then save
      $hashed_pass = AuthComponent::password($this->request->data['User']['current_password']);
      $user_info = $this->User->read(null, $loggedInUserId);

      $user_saved = false;
      if ($hashed_pass === $user_info['User']['password'] && $this->request->data["User"]["password"] === $this->request->data["User"]["confirm_password"]) {
        $this->User->id = $id;
        $user_saved = $this->User->saveField("password", $this->request->data["User"]["password"]);
      }

      if ($user_saved) {
        $this->Session->setFlash(__('Password updated!'));
        $this->redirect(array('action' => 'dashboard'));
      } else {
        $this->request->data['User']['current_password'] = null;
        $this->request->data['User']['password'] = null;
        $this->request->data['User']['confirm_password'] = null;
        $this->Session->setFlash(__("Could not update password. Ensure that you've entered your current password correctly and that the both new password fields match" ));
      }
    }
  }

  public function add() {
    if ($this->request->is('post')) {
        $this->User->create();
        if ($this->User->save($this->request->data)) {
            $this->Session->setFlash(__('The user has been saved'));
            $this->redirect(array('action' => 'manage'));
        } else {
            $this->Session->setFlash(__('The user not yet created. Please correct the errors noted below and try again.'));
        }
    }

    $this->setHosts();
  }

  public function edit($id = null) {
      $this->set("user_id", $id);
      $this->User->id = $id;
      if (!$this->User->exists()) {
          throw new NotFoundException(__('Invalid user'));
      }
      if ($this->request->is('post') || $this->request->is('put')) {
          if ($this->User->save($this->request->data)) {
              $this->Session->setFlash(__('The user has been saved'));
              $this->redirect(array('action' => 'manage'));
          } else {
              $this->Session->setFlash(__('The user could not be saved. Please correct the errors noted below and try again.'));
          }
      } else {
          $this->request->data = $this->User->read(null, $id);
          unset($this->request->data['User']['password']);
      }

      $this->setHosts();
  }

  public function delete($id = null) {
      if (!$this->request->is('post')) {
          throw new MethodNotAllowedException();
      }

      $this->User->id = $id;
      if (!$this->User->exists()) {
          throw new NotFoundException(__('Invalid user'));
      }

      // let's delete all of the volunteer spots under the project
      $this->loadModel('VolunteerSpot');
      $this->VolunteerSpot->deleteAll(array('VolunteerSpot.user_id' => $id), false);

      // TODO: all of the cached volunteer counts will be off, need to update those for each project

      if ($this->User->delete()) {
          $this->Session->setFlash(__('User deleted'));
          $this->redirect(array('action' => 'manage'));
      } else {
        $this->Session->setFlash(__('User could not be deleted. Please try again.'));
        $this->redirect(array('action' => 'manage'));
      }
  }

  public function volunteer_spot() {
    if (!$this->request->is('post')) {
        throw new MethodNotAllowedException();
    }

    $this->loadModel('VolunteerSpot');
    $spot = $this->volunteerSpotForUser($this->Auth->user('id'));
    if ($spot === null) {
      $this->redirect(array('action' => 'dashboard'));
    } else {
      foreach( $spot as $s ) {
        $this->VolunteerSpot->id = $s['VolunteerSpot']['id'];
        $this->VolunteerSpot->delete();
        // update project totals
        $project_id = $s['VolunteerSpot']['project_id'];
        $this->updateTotalVolunteerCountForProject($project_id);

        $this->Session->setFlash(__('Your volunteer spot was given up. Be sure to sign up for a new project!'));
        $this->redirect(array('action' => 'dashboard'));
      }
    }
  }
}

?>
