<?php

function startsWith($haystack, $needle)
{
    return !strncmp($haystack, $needle, strlen($needle));
}

App::uses('AppHelper', 'View/Helper');
class SSHelper extends AppHelper {

  public function webURL($url = null) {
    if ($url === null) {
      return "";
    } else if (startsWith($url, "http")) {
      return $url;
    } else {
      return "http://".$url;
    }
  }

  public function isAllAges($project = null) {
    $p = $project['Project'];
    return $p['minimum_volunteer_age'] === null;
  }

  public function isGeolocated($project = null) {
    $p = $project['Project'];
    return (isset($p['latitude']) && $p['latitude'] !== null && isset($p['longitude']) && $p['longitude'] !== null);
  }

  public function formattedLatLong($project) {
    if (!$this->isGeolocated($project)) {
      return "-1000, -1000";
    } else {
      $p = $project['Project'];
      return $p['latitude'].", ".$p['longitude'];
    }
  }

  public function hasAddress($project = null) {
    $p = $project['Project'];
    return (isset($p['address_line_1']) && $p['address_line_1'] !== "" && isset($p['city']) && $p['city'] !== "" && isset($p['state']) && $p['state'] !== "");
  }

  public function hasMaterialsNeeded($project = null) {
    $p = $project['Project'];
    return (isset($p['materials_needed']) && $p['materials_needed'] !== "");
  }

  public function hasSpecialInstructions($project = null) {
    $p = $project['Project'];
    return (isset($p['special_instructions']) && $p['special_instructions'] !== "");
  }

  public function formattedCityAndState($project = null) {
    if (!$this->hasAddress($project)) {
      return "No address given";
    } else {
      $p = $project['Project'];
      return $p['city'].", ".$p['state'];
    }
  }

  public function isRegistrationClosed($project = null) {
    if ($project === null) {
      return true;
    } else {
      $earliestCloseDate = min(strtotime($project["Project"]["close_date"]), strtotime($project["Project"]["date"]));
      return (time() > $earliestCloseDate);
    }
  }

  public function areas() {
    return array(1 => "St. Louis City", 2 => "North County", 3 => "Northwest County", 4 => "West County", 5 => "South County", 6 => "Metro East", 7 => "St. Charles County", 8 => "Franklin County", 9 => "Jefferson County", 10 => "St. Louis County - Other");
  }

  public function additionalVolunteersForSpot($volunteerSpot = null) {
    if ($volunteerSpot === null) {
      return array();
    } else {
      $nameDelimiter = "*|*";
      $names = explode("\n", $volunteerSpot["VolunteerSpot"]['family_info']);
      for ($i = 0; $i < count($names); $i++) {
        $names[$i] = str_replace($nameDelimiter, " ", $names[$i]);
      }
      array_pop($names);
      return $names;
    }
  }
}

?>
