<?php

class BSHelper extends AppHelper {
  var $helpers = array('Form');
  
  public function create($model = null) {
    return $this->Form->create($model, array(
      'class' => 'form-horizontal',
      'inputDefaults' => array(
        'div' => array('class' => 'control-group'),
        'label' => array('class' => 'control-label'),
        'between' => '<div class="controls">',
        'after' => '</div>',
        'class' => ''
      )
    ));
  }
  
  public function end($options = null) {
    return $this->Form->end($options);
  }
}

?>