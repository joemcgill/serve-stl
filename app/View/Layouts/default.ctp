<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title_for_layout; ?> | Serve St. Louis</title>
	<link href='https://fonts.googleapis.com/css?family=Fjalla+One|Oxygen:400,300' rel='stylesheet' type='text/css'>
	<?php
  	echo $this->fetch('meta');

		echo $this->Html->css('normalize');
		echo $this->Html->css('main');
		echo $this->Html->css('anythingslider');
		echo $this->Html->css('animate');

		echo $this->fetch('css');
	?>

	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->webroot; ?>favicon.ico">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $this->webroot; ?>Icon@2x.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $this->webroot; ?>Icon-72.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo $this->webroot; ?>Icon-Small.png">

  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
</head>
<body>

	<div class="container">

      <div id="navbar">

        <ul class="nav drop">
          <li><?php echo $this->Html->link('Home', array('controller' => 'pages', 'action' => 'display', 'home')); ?></li>
          <li><?php echo $this->Html->link('Projects', array('controller' => 'projects', 'action' => 'search')); ?></li>
         <?php foreach ($pages as $page): ?>
          <?php if ( $page['Page']['is_active'] ) : ?>
            <li>
              <?php echo $this->Html->link($page['Page']['title'], array('controller' => 'pages', 'action' => 'show', $page['Page']['slug'])); ?>
              <?php if (count($page['children']) > 0): ?>
                <ul class="sub-nav">
                  <?php foreach ($page['children'] as $subpage): ?>
                    <?php if ($subpage['is_active']): ?>
                      <li><?php echo $this->Html->link($subpage['title'], array('controller' => 'pages', 'action' => 'show', $subpage['slug'])); ?></li>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </ul>
              <?php endif; ?>
            </li>
          <?php endif; ?>
          <?php endforeach; ?>
          <li class="volunteer_line"><span class="volunteer_count"><?php echo $totalVolunteerCount; ?></span>&nbsp; serving!</li>
        </ul>

       <ul class="social">
         					<li><a href="https://www.facebook.com/ServeStLouis" alt="Visit Serve St. Louis on Facebook" target="_blank" title="Visit Serve St. Louis on Facebook"><span class="socicon">b</span></a></li>


					<li><a href="https://twitter.com/ServeStLouis" target="_blank" alt="Visit Serve St. Louis on Twitter" title="Visit Serve St. Louis on Twitter"><span class="socicon">a</span></a></li>
				</ul>
      </div>




      <div id="logo">

 			<div id="login-bar">

      	<ul>
        <?php if ($this->Session->check('Auth.User.id')): ?>
           <li class="hello-name">Hi, <?php echo $this->Session->read('Auth.User.first_name'); ?> </li><li> <?php echo $this->Html->link('View Dashboard', array('controller' => 'users', 'action' => 'dashboard')); ?> </li><li> <?php echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout')); ?> </li>
         <?php else: ?>

           <li class="first-child-login"><?php echo $this->Html->link('Login', array('controller' => 'users', 'action' => 'login')); ?> </li>
            <li class="first-child-login"><?php echo $this->Html->link('1st Time Users', array('controller' => 'users', 'action' => 'signup')); ?> </li>
         <?php endif; ?>
      	</ul>

     </div>

      <!--  <a href="<?php echo $this->Html->url(array('controller' => 'projects', 'action' => 'search'));?>" class="airplane"><img src="<?php echo $this->webroot; ?>img/airplane-animation.gif" /></a>  -->



        <div id="datebar">
  	      <span>Save the Date: Oct. 7 &amp; 8, 2017!</span>


        </div>
      </div>
   <div id="countdown"></div>

		<div id="content">
			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>

  	<div id="footer">
  	  <p class="copyright">&copy; 2013 | Digital Owl Interactive</p>
  	  <p class="contact">Feel free to email SERVE ST LOUIS with any questions | <a id="info-email" href="mailto:info@servestlouis.com">info@servestlouis.com</a></p>
  	</div>

	</div>

	<? /* php echo $this->element('sql_dump'); */ ?>
	<?php
		echo $this->Html->script('jquery.easing.1.2');
		echo $this->Html->script('swfobject');
		echo $this->Html->script('jquery.anythingslider');
		echo $this->Html->script('showHide');
		echo $this->Html->script('jquery.lightbox_me');

    echo $this->fetch('script');
	?>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38539706-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<script>
			$('.first').addClass('animated fadeInUp');
				setTimeout(function () {
				    $('.second').show().addClass('animated fadeInRightBig');}, 500
				);
					</script>


					<script>

    CountDownTimer('10/7/2017 07:00 AM', 'countdown');


    function CountDownTimer(dt, id)
    {
        var end = new Date(dt);

        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;

        function showRemaining() {
            var now = new Date();
            var distance = end.getTime()-now.getTime();
            if (distance < 0) {

                clearInterval(timer);
                var output = "";

                return;
            }
            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);

            document.getElementById(id).innerHTML = days + ' days <br /> ';
            document.getElementById(id).innerHTML += hours + ' hrs <br /> ';
            document.getElementById(id).innerHTML += minutes + ' mins <br /> ';
            document.getElementById(id).innerHTML += seconds + ' secs';
        }

        timer = setInterval(showRemaining, 1000);
    }

</script>

</body>
</html>
