<h2>Thanks for subscribing to our newsletter!</h2>
<p>If at any time you wish to stop receiving our newsletters, be sure to follow the 'unsubscribe' link in one of newsletter emails!</p>