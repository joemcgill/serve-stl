<?php $this->set('title_for_layout', $project['Project']['title']); ?>

<style>
#logo {
  	background: url('<?php echo $this->webroot; ?>img/serveprojects-logo.png') no-repeat !important;
  	height: 200px !important;
		width: 960px;
		margin: 0px;
		position: relative;
}

.airplane {
	display: none;
}

form.volunteer-form {
  padding: 0px;
}

</style>
<?php
function ellipsis($s, $max_length) {
  if (strlen($s) > $max_length) {
      $offset = ($max_length - 3) - strlen($s);
      return substr($s, 0, strrpos($s, ' ', $offset)) . '...';
  } else {
    return $s;
  }
}

function percentage($current, $total) {
  return min(100, floor(100 * ($current / $total)));
}

?>
<div class="row">
  <?php echo $this->Session->flash(); ?>
  <h1 class="super-project"><?php echo $project['Project']['title']; ?></h1>
  <hr />
</div>

<div class="row">
  <div class="project_page_image">
     <?php if (isset($project['Project']['image_path']))  {
              echo '<img class="project_image" style="float:right;" src="' . $this->webroot . $project['Project']['image_path'] . '" />';
            } else if (isset($project['Host']['image_path'])) {
              echo '<img class="project_image" style="float:right;" src="' . $this->webroot . $project['Host']['image_path'] . '" />';
            } else {
              echo '<img src="' . $this->webroot . 'img/serve_stl_project_no_image.jpg" class="project_image" width="500" height="250" style="float:right;" />';
            }
            ?>
  </div>

  <div class="social-share">
    <div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div><div style="margin-left: 24px; display: inline-block;"><a href="https://twitter.com/share" class="twitter-share-button">Tweet</a></div>
  </div>


  <div class="project-info">
    <img src="<?php echo $this->webroot; ?>img/time_icon.png" style="float:left; padding:3px 10px;"/>
    <p><time datetime="<?php echo $project['Project']['date']; ?>"><?php echo $this->Time->format("M jS, Y @ g:ia", $project['Project']['date']); ?></time></p>



  <?php if ($this->SS->hasAddress($project)): ?>
    <img src="<?php echo $this->webroot; ?>img/location_icon.png" style="float:left; padding:10px;"/>
    <address class="project-location">
      <p><?php echo $project['Project']['address_line_1']; ?></p>
    <?php if (isset($project['Project']['address_line_2'])): ?>
      <p><?php echo $project['Project']['address_line_2']; ?></p>
    <?php endif; ?>
      <p><?php echo $this->SS->formattedCityAndState($project); ?> <?php echo $project['Project']['zipcode']; ?></p>
    </address>
  <?php endif; ?>

    <ul class="horizontal-list project_type_icons">
              <?php if ($project['Project']['outdoor']): ?>
                <li><img src="<?php echo $this->webroot; ?>img/outdoor_icon.png" /> Outdoor</li>
              <?php endif; ?>
              <?php if ($project['Project']['indoor']): ?>
                <li><img src="<?php echo $this->webroot; ?>img/indoor_icon.png" /> Indoor</li>
              <?php endif; ?>
              <?php if ($project['Project']['physical_labor']): ?>
                <li><img src="<?php echo $this->webroot; ?>img/physical_labor_icon.png" /> Physical Labor</li>
              <?php endif; ?>
              <?php if ($project['Project']['skilled']): ?>
                <li><img src="<?php echo $this->webroot; ?>img/skilled_labor_icon.png" /> Skilled Labor</li>
              <?php endif; ?>
              <?php if ($project['Project']['family_friendly']): ?>
                <li><img src="<?php echo $this->webroot; ?>img/family_friendly_icon.png" /> Family Friendly</li>
              <?php endif; ?>

      </ul>

	  <p><?php if ($project['Project']['minimum_volunteer_age'] === "13"): ?>
             <p >Ages: 13+</p>
            <?php endif; ?></p>
      <p><?php if ($project['Project']['minimum_volunteer_age'] === "16"): ?>
             <p>Ages: 16+</p>
             <?php endif; ?></p>
      <p><?php if ($project['Project']['minimum_volunteer_age'] === "18"): ?>
             <p>Ages: 18+</p>
            <?php endif; ?></p>
      <p><?php if ($project['Project']['minimum_volunteer_age'] === "21"): ?>
             <p>Ages: 21+</p>
            <?php endif; ?></p>
	  <p><?php if ($project['Project']['minimum_volunteer_age'] === null): ?>
             <p>All Ages</p>
            <?php endif; ?></p>


            <div id="progress_bar_container">
            <p class="highlight">Number of volunteers signed up:</p>
            <div class="progress_bar-show">
              <span class="progress-show" style="width:<?php echo percentage($project['Project']['volunteer_count'], $project['Project']['number_of_volunteers_needed']); ?>%"></span>
            </div>
            <span class="empty-show">E</span>
            <span class="people_needed-show"><?php echo $project['Project']['volunteer_count']; ?> / <?php echo $project['Project']['number_of_volunteers_needed']; ?> volunteers</span>
            <span class="full-show">F</span>
            </div>

      <p><span class="highlight">Hosted by:</span> <a href="<?php echo $this->SS->webURL($project['Host']['website_url']); ?>" target="_blank"><?php echo $project['Host']['name']; ?></a></p>
      <p><span class="highlight">Project Leader:</span>
      <br /><?php echo $project['User']['name']; ?>
      <br />Email: <a href="mailto:<?php echo $project['User']['email']; ?>"><?php echo $project['User']['email']; ?></a>
      <br />Phone: <?php echo $project['User']['phone_number']; ?>
      </p>
      <p><span class="highlight">Description:</span>
      <br />
      <?php echo $project['Project']['description']; ?></p>

    <?php if ($this->SS->hasMaterialsNeeded($project)): ?>
      <p><span class="highlight">Materials needed:</span> <?php echo $project['Project']['materials_needed']; ?></p>
    <?php else: ?>
      <p><span class="highlight">Materials needed:</span> N/A</p>
    <?php endif; ?>

    <?php if ($this->SS->hasSpecialInstructions($project)): ?>
      <p><span class="highlight">Special instructions:</span> <?php echo $project['Project']['special_instructions']; ?></p>
    <?php else: ?>
      <p><span class="highlight">Special instructions:</span> N/A</p>
    <?php endif; ?>
  </div>

  <hr />

<?php if ($this->SS->isRegistrationClosed($project)): ?>
  <div class="signup-form center">
    <h2>Thanks for your interest in Serve St. Louis!</h2>
    <p>Web registration for this project has closed.  If you’d like to ask about registering for this project, please contact the project leader/host church directly.</p>
  </div>
<?php elseif ($project["Project"]["volunteer_count"] >= $project["Project"]["number_of_volunteers_needed"]): ?>
  <div class="signup-form center">
    <h2>This project is full!</h2>
    <p>There are a lot of great projects to choose from. Go to our search page to locate exciting projects going on around you!</p>
  </div>
<?php elseif (!$this->Session->check('Auth.User.id')): ?>
  <div class="signup-form center">
    <h2>Want to volunteer for this project?</h2>
      <div style="text-align: center;">
        <div class='btn-full' style="display: inline;"><?php echo $this->Html->link('Login', array('controller' => 'users', 'action' => 'login', '?' => array("redirect_to_project" => $project["Project"]["id"]))); ?></div> &nbsp;&nbsp;&nbsp; <em style="color: #dfdfdf;">or</em> &nbsp;&nbsp;&nbsp; <div class='btn-full' style="display: inline;"><?php echo $this->Html->link('Register', array('controller' => 'users', 'action' => 'signup', '?' => array("redirect_to_project" => $project["Project"]["id"]))); ?></div>
        <div class="clearfix" style="height: 12px;"></div>
      </div>
    </div>
<?php
// This person may be able to sign up, as long as they're not already double booked.
else :
  $conflict = false;
  /*
   * First we see if this person is already signed up for a project. If so, we
   * look to see if they're already signed up for this project or if they have a
   * confilct with another project at the same start time.
   */
  if ( $volunteerSpot !== null ) {

    $project_time = strtotime( $project['Project']['date'] );

    foreach( $volunteerSpot as $v ) {
      // Calculate the time difference between projects.
      $time_diff = abs( strtotime($v['Project']['date']) - $project_time );
      if ( $v['VolunteerSpot']['project_id'] === $project['Project']['id'] || 7200 > $time_diff ) {
        $conflict = $v['VolunteerSpot']['project_id'];
      }
    }
  }

  if ( $conflict ) {
    $form_title = ( $project['Project']['id'] === $conflict ) ? 'You are signed up for this project!' : 'You have a conflict.';
    $form_message = ( $project['Project']['id'] === $conflict ) ?  'We are looking forward to volunteering with you this Serve Day!' : 'Looks like you&lsquo;re already volunteerning during this time. If you want to make changes to your volunteer efforts, please visit&nbsp;the&nbsp;<a href="/users/dashboard/">dashboard</a>.';
  ?>
    <div class="signup-form center">
      <h2><?php echo $form_title; ?></h2>
      <p><?php echo $form_message; ?></p>
    </div>
  <?php } else { ?>
  <div class="signup-form center">
    <h2>Want to volunteer for this project?</h2>
      <div>
        <?php echo $this->Form->create('VolunteerSpot', array('class' => 'volunteer-form')); ?>
        <p style="font-size:14px;">Click the button at the bottom of this form to sign up for this project.</p>
        <h3 style="margin-bottom:10px;">Your name goes here:</h3>
        <span style="width:180px;">Your first name:</span><span style="margin-left:53px;">Your last name:</span><br />
        <div style="margin-top:-20px; border-bottom: 1px dashed grey;">
          <p>
            <input placeholder="Your First Name" type="text" class="first-name" />
            <input placeholder="Your Last Name" type="text" class="last-name"  />
          </p>
        </div>

        <p style="font-size:14px;">If you have any additional family members or volunteers (not including yourself) you would like to sign up for this project under <strong>your login info</strong> place their names below.</p>
        <p style="font-size:14px; color: orange;"><strong>Important:</strong> The additional names are for people who are in your family and doing the same project or for youth groups being signed up by a youth group leader. If you have other adults, please have them sign up for the project on their own email account.</p>
        <h3 style="font-size:21px;">Family members or Youth Group members below:</h3>
        <span style="width:180px;">First name:</span><span style="margin-left:90px;">Last name:</span><br />
        <div id="firstlastnames">
          <p>
            <input name="firstName_1" id="firstName_1" value="" placeholder="Additional First Name" type="text" class="first-name" title="First name" />
            <input name="lastName_1" id="lastName_1" value="" placeholder="Additional Last Name" type="text" class="last-name" title="Last name" />
            <a id="add-field" href="#"><img src="<?php echo $this->webroot; ?>img/add_icon.png" style="margin-top:-8px;" /></a>
          </p>
        </div>

        <?php echo $this->Form->hidden('total_volunteers', array("value" => "1")) ?>
        <?php echo $this->Form->hidden('family_info') ?>

        <div style="border-top:1px dashed grey; padding-top:10px;">
          <?php echo $this->Form->input('message_to_project_leader', array('type' => 'textarea', 'rows' => '4', 'cols' => '55')); ?>
        </div>

        <div style="height: 10px;"></div>
          <?php echo $this->Form->end(__('Sign up to volunteer'));?>

        <!-- used as a template. not visible! -->
        <p id="firstlastnames-template" style="display: none;">
          <input name="firstName_" id="firstName_" value="" placeholder="Additional First Name" type="text" class="first-name" title="First name" />
          <input name="lastName_" id="lastName_" value="" placeholder="Additional Last Name" type="text" class="last-name" title="Last name" />
          <a id="remove-field" href="#"><img src="<?php echo $this->webroot; ?>img/minus_icon.png" style="margin-top:-8px;" /></a>
        </p>
      </div>
  </div>
  <?php } ?>
<?php endif; ?>
<?php if ($this->SS->isGeolocated($project)): ?>
  <hr />
  <div id="map_canvas" style="width: 840px; height:400px; margin:0 auto;"></div>
<?php endif;?>

<?php if ($canViewVolunteers): ?>
  <!-- BEGIN Admin Section for Viewing Volunteers who have signed up -->

  <hr />

  <h2>Admin functions for this project.</h2>
  <br />
  <div class="btn-build" style="display:inline-block;"><button><?php echo $this->Html->link(
          'Build export',
          array('controller' => 'projects', 'action' => 'export', $project['Project']['id']),
          array('class' => 'white')
        ); ?></button></div>
  <br />
 <div class="btn-full" style="text-align:left;"><button id="email-members">Email Volunteers</button></div>

  <table class="table table-striped small">
  <tbody>
  	<thead>
  		<tr>
  			<th>Volunteer</th>
  			<th class="span4">Email</th>
  			<th>Phone</th>
  			<th>Remove</th>

  		</tr>
  	</thead>
  <?php foreach ($volunteerSpots as $volunteer): ?>
    <tr id="volunteer-info">
      <td><?php echo $volunteer["User"]["name"]; ?></td>
      <td><span class="member-email"><?php echo $volunteer["User"]["email"]; ?><span></td>
      <td><?php echo $volunteer["User"]["phone_number"]; ?></td>
      <td class="span1" style="text-align:center"><?php echo $this->Form->postLink(
          'X',
          array('action' => 'delete_volunteer', $volunteer['VolunteerSpot']['id']),
          array('confirm' => 'Are you sure?', 'class' => 'btn btn-danger')
        ); ?></td>


     	 <tr>
     	 	<td style="margin-left:10px;">
          <?php foreach ($this->SS->additionalVolunteersForSpot($volunteer) as $family_member): ?>
            - <?php echo $family_member; ?><br />
          <?php endforeach; ?>
        </td>
     	 	<td colspan="2">
          <?php if (!empty($volunteer["VolunteerSpot"]["message_to_project_leader"])): ?>
          <strong>Message from Volunteer:</strong>
	     	 	<br />
            <?php echo $volunteer["VolunteerSpot"]["message_to_project_leader"]; ?>
          <?php else: ?>

          <?php endif; ?>
        </td>
     	 	<td></td>
	 	   </tr>
    </tr>
  <?php endforeach; ?>

  </tbody>
</table>

<script>
$("#email-members").click(function(e) {
  e.preventDefault();

  var mailto = "mailto:";
  $(".member-email").each(function() {
    mailto = mailto + ($(this).text() + ";");
  });
  window.location = mailto;
});
</script>

<!-- END Admin Section for Viewing Volunteers who have signed up -->
<?php endif; ?>

  <div class="clearfix"></div>


<?php if ($this->SS->isGeolocated($project)): ?>
  <br />


  <script>
  function initialize() {
    var latLng = new google.maps.LatLng(<?php echo $project['Project']['latitude'] ?>, <?php echo $project['Project']['longitude'] ?>);
    var mapOptions = {
      zoom: 13,
      center: latLng,
      scrollwheel:false,
      draggable:false,
      mapTypeId: google.maps.MapTypeId.ROADMAP

    }
    var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

    new google.maps.Marker({
      position: latLng,
      map: map,
      icon: 'http://www.servestlouis.org/app/webroot/img/serve-pin.png',
    });
  }

  function loadScript() {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAnZ0XIoVpHBe5rxExiRKe20KdnsftPiOE&sensor=false&callback=initialize";
    document.body.appendChild(script);
  }



  $(function() {
    loadScript();
  });

  </script>
<?php endif;?>

</div>

<!--script to allow additional fields to be created for names of volunteers signing up -->

<script>
$(function() {
  var scntDiv = $('#firstlastnames');
  var i = $('#firstlastnames p').size() + 1;

  $('#add-field').live('click', function() {
    var clone = $('#firstlastnames-template').clone()
      .attr("style", "")
      .attr('id', 'firstlastnames_' + i);

    clone.children(".first-name").attr('id', 'firstName_' + i).attr('name', 'firstName_' + i);
    clone.children(".last-name").attr('id', 'lastName_' + i).attr('name', 'lastName_' + i);

    clone.appendTo(scntDiv);
    i++;
    return false;
  });

  $('#remove-field').live('click', function() {
    if (i > 2) {
      $(this).parent().remove();
      i--;
    }
    return false;
  });
});
</script>
