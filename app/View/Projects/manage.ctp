<?php $this->set('title_for_layout', 'Manage Projects'); ?>
<div class="row">
  <h1 class="super">Manage Projects</h1>
  <hr />
</div>
<div class="row">
<div class="span12">
<p style="float:left;"><?php echo $this->Html->link('+ Add new project', array('action' => 'edit')); ?></p>
<?php echo $this->Session->flash(); ?>
<p class="pull-right"><?php echo count($projects); ?> total projects</p>
<table class="table table-striped">
  <tbody>
  	<thead>
  		<tr>
  			<th>Project Name</th>
  			<th>Host</th>
  			<th>Volunteers</th>        
  			<th>Manage</th>
  		</tr>
  	</thead>
  <?php foreach ($projects as $project): ?>
    <tr>
      <td><?php echo $this->Html->link($project['Project']['title'], array('action' => 'show', $project['Project']['id'])) ?></td>
      <td><?php echo $project['Host']['name']; ?></td>
      <td><?php echo $project['Project']['volunteer_count']; ?> / <?php echo $project['Project']['number_of_volunteers_needed']; ?></td>
      <td>
        <?php echo $this->Html->link(
          'Edit',
          array('action' => 'edit', $project['Project']['id']),
          array('class' => 'btn')
        ); ?>
        &nbsp;&nbsp;
        <?php echo $this->Form->postLink(
          'Delete', 
          array('action' => 'delete', $project['Project']['id']),
          array('confirm' => 'Are you sure?', 'class' => 'btn btn-danger')
        ); ?>
      </td>
    </tr> 
  <?php endforeach; ?>
  </tbody>
</table>
</div>
</div>