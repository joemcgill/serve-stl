<?php $this->set('title_for_layout', "Success!"); ?>
<div class="row">

  <h1 class="super">Thanks for signing up!</h1>
  <hr />
</div>

<div class="row">
   
  <h2 class="center">We are so happy that you're choosing to Serve St. Louis.</h2>
  <br />
  <div class="span3" style="margin-left:-20px;"></div>
   <div class="span6 center shirt">
   		<a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'dashboard')); ?>"><img src="<?php echo $this->webroot; ?>img/dashboard_icon.png" class="center" />
	   <p>You can now visit <?php echo $this->Html->link('your DASHBOARD', array('controller' => 'users', 'action' => 'dashboard')); ?> to see the project you've signed up for.</p>
   		</a>
   </div>
  <div class="span1"></div>
 
 

  <div class="span6 center shirt">
  	<a href="<?php echo $this->Html->url(array('controller' => 'projects', 'action' => 'search')); ?>"><img src="<?php echo $this->webroot; ?>img/manage_projects.png" class="center" /></a>
  	<p>Or, if you'd like to search for some other project opportunities, visit the <?php echo $this->Html->link('PROJECTS PAGE', array('controller' => 'projects', 'action' => 'search')); ?>.</p>
  </div>
  
</div>
  <div class="clearfix"></div>
<div class="row">
	<div class="span1"></div>
	<div class="span11">
  <h1 class="center">Thank you again for your willingness to help out!</h1>
	</div>
</div>


<br />



