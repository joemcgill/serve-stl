<?php $this->set('title_for_layout', "Find a project"); ?>

<style>
#logo {
  	background: url('<?php echo $this->webroot; ?>img/serveprojects-logo.png') no-repeat !important;
		height: 200px !important;
		width: 960px;
		margin: 0px;
		position: relative;
}

.airplane {
	display: none;
}

.project-search {
  padding: 10px 0px;
}
ul.filters {
  margin: 0px 0px;
}
</style>

<?php
function ellipsis($s, $max_length) {
  if (strlen($s) > $max_length) {
      $offset = ($max_length - 3) - strlen($s);
      return substr($s, 0, strrpos($s, ' ', $offset)) . '...';
  } else {
    return $s;
  }
}

function percentage($current, $total) {
  return min(100, floor(100 * ($current / $total)));
}

?>

<!-- BEGIN PROJECT LIST SECTION -->
<div class="row" style="width:1000px; margin-left:-59px;">
	<div class="span4" style="margin-left:0px;">
	<div id="top-fold"></div>
	<div id="project_filter_bar">
		<h1><span>Find a project to take part in. <br />Use our filters to narrow your search!</span></h1><hr />
    <?php echo $this->Form->create(false, array('type' => 'get', 'class' => 'project-search', 'url' => '/projects/search')); ?>
		<ul class="filters">
      <li class="keyword_search">
        <h2>Filter by keyword</h2>
        <input name="keyword" id="keyword" type="text" />
      </li>
      <li class="project_host">
        <h2>Host</h2>
          <?php echo $this->Form->select('host', $hosts, array('empty' => 'All hosts')); ?>
      </li>
			<li class="project_type">
			<h2>Project Type</h2>
				<div id="characteristics">
        <?php echo $this->Form->checkbox('family_friendly');?><img src="<?php echo $this->webroot; ?>img/family_friendly_icon.png" /> Family Friendly<br>
        <?php echo $this->Form->checkbox('skilled');?><img src="<?php echo $this->webroot; ?>img/skilled_labor_icon.png" /> Skilled <br />
        <?php echo $this->Form->checkbox('physical_labor');?><img src="<?php echo $this->webroot; ?>img/physical_labor_icon.png" /> Physical Labor <br />
        <?php echo $this->Form->checkbox('indoor');?><img src="<?php echo $this->webroot; ?>img/indoor_icon.png" /> Indoor <br />
        <?php echo $this->Form->checkbox('outdoor');?><img src="<?php echo $this->webroot; ?>img/outdoor_icon.png" /> Outdoor <br />

				</div>

			</li>
			<li class="project_date">
			<h2>Project Date</h2>
      <?php
        echo $this->Form->select('date',
          array("1" => "October 7, 2017", "2" => "October 8, 2017"),
          array('empty' => 'All dates'));
      ?>
			</li>
			<li class="project_age">
			<h2>Minimum Age</h2>
      <?php
        echo $this->Form->select('minimum_volunteer_age',
          array("13" => "13+", "16" => "16+", "18" => "18+", "21" => "21+"),
          array('empty' => 'All ages'));
      ?>
			</li>
			<li class="project_other">
			<h2>Area</h2>
        <?php echo $this->Form->select('area',
            $this->SS->areas(),
            array('empty' => 'All areas'));
        ?>
			</li>
			<li class="switch_to_map">
  			<h2>View Mode</h2>
  			<img src="<?php echo $this->webroot; ?>img/switch_to_map_button.png" />
        <div style="height: 6px;"></div>
        <?php echo $this->Form->select('map',
            array(1 => "View projects on a map"),
            array('empty' => 'View projects as a list'));
        ?>
			</li>
		</ul>

    <div class="clearfix"></div>
    <hr />
    <div class="btn-secondary"><button id="clear-fields">Clear All Fields</button></div>
    <div class="btn-full"><button>Find Projects</button></div>


    <div class="clearfix"></div>
    <?php echo $this->Form->end(); ?>

  </div><div id="bottom-fold"></div>

	</div> <!--end span3-->
	<div class="span9" style="width:700px;" >
  		<ul class="project_cards" <?php if (isset($this->data['map']) && $this->data['map'] == 1){ echo "style = 'display: none;'"; } ?> >
      <?php foreach ($projects as $project): ?>
  			<li class="project_card" id="project-<?php echo $project['Project']['id']; ?>">
          <a href="<?php echo $this->Html->url(array('controller' => 'projects', 'action' => 'show', $project['Project']['id'])); ?>">
          <div class="project-image-thumb">

            <?php
            if (isset($project['Project']['image_path']))  {
              echo '<img src="' . $this->webroot . $project['Project']['image_path'] . '" />';
            } else {
              echo '<img src="' . $this->webroot . 'img/serve_stl_project_no_image_small.jpg" />';
            }
            ?></div>
            <h3><?php echo $project['Project']['title']; ?></h3>
            <h4><?php echo $project['Host']['name']; ?></h4>
            <h4><?php echo $this->SS->formattedCityAndState($project); ?></h4>
            <div class="card-body"><p class="card-description"><?php echo ellipsis($project['Project']['description'], 180); ?></p></div>

            <span class="time-stamp" time="<?php echo $project['Project']['date']; ?>"><?php echo $this->Time->format("M jS, Y @ g:ia", $project['Project']['date']); ?></span>

            <div class="project-attributes">
		            <hr />

		            <?php if ($this->SS->isAllAges($project)): ?>
		              <img src="<?php echo $this->webroot; ?>img/ages_any_icon.png" class="ages" />
		            <?php endif; ?>
		            <ul class="project_type_icons">
		              <?php if ($project['Project']['outdoor']): ?>
		                <li><img src="<?php echo $this->webroot; ?>img/outdoor_icon.png" /></li>
		              <?php endif; ?>
		              <?php if ($project['Project']['indoor']): ?>
		                <li><img src="<?php echo $this->webroot; ?>img/indoor_icon.png" /></li>
		              <?php endif; ?>
		              <?php if ($project['Project']['physical_labor']): ?>
		                <li><img src="<?php echo $this->webroot; ?>img/physical_labor_icon.png" /></li>
		              <?php endif; ?>
		              <?php if ($project['Project']['skilled']): ?>
		                <li><img src="<?php echo $this->webroot; ?>img/skilled_labor_icon.png" /></li>
		              <?php endif; ?>
		              <?php if ($project['Project']['family_friendly']): ?>
		                <li><img src="<?php echo $this->webroot; ?>img/family_friendly_icon.png" /></li>
		              <?php endif; ?>
		            </ul>
		            <div class="progress_bar">
		              <span class="progress" style="width:<?php echo percentage($project['Project']['volunteer_count'], $project['Project']['number_of_volunteers_needed']); ?>%"></span>
		            </div>
		            <span class="empty">E</span>
		            <span class="people_needed"><?php echo $project['Project']['volunteer_count']; ?> / <?php echo $project['Project']['number_of_volunteers_needed']; ?> volunteers</span>
		            <span class="full">F</span>
            </div>
          </a>
        </li>
      <?php endforeach; ?>
  		</ul>
    <?php if (isset($this->data['map']) && $this->data['map'] == 1): ?>
      <div id="map_canvas" class="search-map" style="width: 90%; height: 660px; margin: 10px 0px 10px 30px;"></div>

      <script>
      function initialize() {
        var mapOptions = {
          zoom: 10,
          center: new google.maps.LatLng(38.632427, -90.31929),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        var infoWindow = new google.maps.InfoWindow({content: "Blank"});

        var oms = new OverlappingMarkerSpiderfier(map);
        oms.addListener('click', function(marker, event) {
          infoWindow.close();
          var clone = $("#project-" + marker.projectId).clone().attr("id", "infoWindow");
          var infoContent = $("<ul class='map_cards'></ul>").append(clone);
          infoWindow.setContent(infoContent.outerHTML());
          infoWindow.open(map, marker);
        });

        <?php foreach ($projects as $project): ?>
        <?php if ($this->SS->isGeolocated($project)): ?>
        (function() {
          var m = new google.maps.Marker({
            position: new google.maps.LatLng(<?php echo $this->SS->formattedLatLong($project); ?>),
            map: map,
            icon: 'http://www.servestlouis.org/app/webroot/img/serve-pin.png',
          });
          m.projectId = "<?php echo $project["Project"]["id"]; ?>";
          oms.addMarker(m);
        })();

        <?php endif; ?>
        <?php endforeach; ?>
      }

      $(function() {
        jQuery.fn.outerHTML = function(s) {
            return s
                ? this.before(s).remove()
                : jQuery("<p>").append(this.eq(0).clone()).html();
        };

        initialize();
      });

      </script>
    <?php endif; ?>

    <div class="clearfix"></div>
    <div style="text-align: center;">
      <?php
        echo $this->Paginator->prev();
        echo("&nbsp;&nbsp");
        echo $this->Paginator->numbers();
        echo("&nbsp;&nbsp");
        echo $this->Paginator->next();
      ?>
    </div>

	</div> <!--end span 9 -->
<div class="clearfix"></div>

</div> <!-- END PROJECT LIST SECTION-->
<script>
  $("#clear-fields").click(function(e) {
    e.preventDefault();
    $(".project-search input[type='checkbox']").prop('checked', false);
    $(".project-search select").each(function(index, select) {
      $(select).val($(select).find("option:first").val());
    });
    return false;
  });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnZ0XIoVpHBe5rxExiRKe20KdnsftPiOE&sensor=false"></script>
<?php echo $this->Html->script('oms.min'); ?>
