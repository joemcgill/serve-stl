<div class="row">
  <h1 class="super">Add/Edit Project</h1>
  <hr>
</div>

<?php
function buildNumberArrayUpTo($max, $min = 1) {
  $vals = array();
  for ($i = $min; $i <= $max; $i++) {
    $vals["$i"] = "$i";
  }
  return $vals;
}

echo $this->Form->create("Project", array('type' => 'file'));
echo $this->Form->input('title');

$user = $this->Session->read('Auth.User');
echo $this->Form->input('host_id', array('empty' => true));
echo $this->Form->input('user_id', array('label' => 'Project Leader', 'empty' => true));
?>

<hr />

<p>You can optionally upload a header image for your project. If you would prefer not to provide an image, the default header image of the project's host organization will be used in its place. If the host has not provided an image, a placeholder Serve St. Louis header image will be displayed.</p>
<div class="input class">
  <label for="ProjectImage">Image (recommended size/ratio 500w x 250h (2:1) or larger)</label>
  <?php
  if (isset($this->data['Project']['image_path'])) {
    echo '<img src="' . $this->webroot . $this->data['Project']['image_path'] . '" /><br />';
  }
  echo $this->Form->file('image');
  ?>
</div>

<hr />

<?php
echo $this->Form->input('area', array(
  'options' => $this->SS->areas()
));

echo $this->Form->input('description', array('type' => 'textarea', 'rows' => '10', 'style' => 'width: 90%;'));

if (isset($this->data['Project']['id'])) {
  echo $this->Form->input('date', array('label' => 'Date & Time', 'minYear' => 2017, 'maxYear' => 2017));
} else {
  echo $this->Form->input('date', array('label' => 'Date & Time', 'selected' => '2017-10-07 12:00:00', 'minYear' => 2017, 'maxYear' => 2017));
}
echo $this->Form->input('minimum_volunteer_age', array(
  'options' => array(
    "13" => "13+",
    "16" => "16+",
    "18" => "18+",
    "21" => "21+",
  ),
  'empty' => 'All ages',
  'label' => 'Minimum Volunteer Age (optional)'
));
echo $this->Form->input('number_of_volunteers_needed', array(
  'options' => buildNumberArrayUpTo(500, 1),
  'default' => '25'
));
echo $this->Form->input('materials_needed', array('label' => 'Materials Needed (optional)'));
echo $this->Form->input('special_instructions', array('label' => 'Special Instructions (optional)', 'type' => 'textarea', 'rows' => '10', 'style' => 'width: 90%;'));

echo '<hr />';

echo '<h2>Address Details (optional)</h2>';
echo $this->Form->input('address_line_1');
echo $this->Form->input('address_line_2');
echo $this->Form->input('city');
echo $this->Form->input('state');
echo $this->Form->input('zipcode');

echo '<hr />';
?>

<p>Volunteers can sign up for a project until 11:59pm on the evening of October 6th, 2017, or until the project is full (whichever happens first).</p>
<p>If your project requires you to close volunteer sign-ups earlier, adjust the date below.</p>

<?php
if (isset($this->data['Project']['id'])) {
  echo $this->Form->input('close_date', array('label' => 'Sign-up Cut-off Date', 'minYear' => 2017, 'maxYear' => 2017));
} else {
  echo $this->Form->input('close_date', array('label' => 'Sign-up Cut-off Date', 'selected' => '2017-10-06', 'minYear' => 2017, 'maxYear' => 2017));
}

echo '<hr />';

echo '<h2>Tag your project! (optional)</h2>';
echo $this->Form->input('family_friendly');
echo $this->Form->input('skilled');
echo $this->Form->input('indoor');
echo $this->Form->input('outdoor');
echo $this->Form->input('physical_labor');

echo '<hr />';

echo $this->Form->end(__('Submit'));
?>
