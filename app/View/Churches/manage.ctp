<div class="row">
  <h1 class="super">Manage Churches</h1>
  <hr>
</div>
<p><?php echo $this->Html->link('+ Add new church', array('controller' => 'churches', 'action' => 'edit')); ?></p>
<?php echo $this->Session->flash(); ?>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Name</th>
      <th>City</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($churches as $church): ?>
    <tr>
      <td><?php echo $this->Html->link($church['Church']['name'], array('action' => 'show', $church['Church']['slug'])) ?></td>
      <td><?php echo $church['Church']['city'] ?>, <?php echo $church['Church']['state'] ?></td>
      <td>
        <?php echo $this->Html->link(
          'Edit',
          array('action' => 'edit', $church['Church']['id']),
          array('class' => 'btn')
        ); ?>
        &nbsp;&nbsp;
        <?php echo $this->Form->postLink(
          'Delete', 
          array('action' => 'delete', $church['Church']['id']),
          array('confirm' => 'Are you sure?', 'class' => 'btn btn-danger')
        ); ?>
      </td>
    </tr> 
  <?php endforeach; ?>
  </tbody>
</table>