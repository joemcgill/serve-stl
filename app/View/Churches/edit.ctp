<?php echo $this->BS->create("Church"); ?>

<legend>Add/Edit Church</legend>
<?php
echo $this->Form->input('name');
echo $this->Form->input('phone');
echo $this->Form->input('address_line_1');
echo $this->Form->input('address_line_2');
echo $this->Form->input('city');
echo $this->Form->input('state');
echo $this->Form->input('zipcode');
echo $this->Form->input('website_url');
?>
<div class="form-actions">
  <button type="submit" class="btn btn-primary">Submit</button>
</div>
<?php echo $this->BS->end(); ?>