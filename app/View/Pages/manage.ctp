<div class="row">
  <h1 class="super">Manage Site</h1>
  <hr>
</div>
<div class="row">
<div class="span12">
<h2>Pages</h2>
<p><?php echo $this->Html->link('+ Add new page', array('controller' => 'pages', 'action' => 'edit')); ?></p>
<?php echo $this->Session->flash(); ?>
<ul class="pages">
  <?php foreach ($pages as $page): ?>
    <li data-id="<?php echo $page['Page']['id']; ?>" data-order="<?php echo $page['Page']['order']; ?>">
      <?php echo $page['Page']['title']; ?> (<?php echo $this->Html->link($this->Html->url(array('action' => 'show', $page['Page']['slug'])), array('action' => 'show', $page['Page']['slug'])); ?>) | <?php echo $this->Html->link(
          'Edit',
          array('action' => 'edit', $page['Page']['id']),
          array('class' => 'btn')
        ); ?>
        | 
        <?php echo $this->Form->postLink(
          'Delete', 
          array('action' => 'delete', $page['Page']['id']),
          array('confirm' => 'Are you sure?', 'class' => 'btn btn-danger')
        ); ?>
      <?php if (count($page['children']) > 0): ?>
        <ul class="pages">
          <?php foreach ($page['children'] as $subpage): ?>
            <li data-id="<?php echo $subpage['id']; ?>" data-order="<?php echo $subpage['order']; ?>">
              <?php echo $subpage['title']; ?> (<?php echo $this->Html->link($this->Html->url(array('action' => 'show', $subpage['slug'])), array('action' => 'show', $subpage['slug'])); ?>) | <?php echo $this->Html->link(
                  'Edit',
                  array('action' => 'edit', $subpage['id']),
                  array('class' => 'btn')
                ); ?>
                | 
                <?php echo $this->Form->postLink(
                  'Delete', 
                  array('action' => 'delete', $subpage['id']),
                  array('confirm' => 'Are you sure?', 'class' => 'btn btn-danger')
                ); ?>
            </li>
          <?php endforeach; ?>                                
        </ul>
      <?php endif; ?> 
    </li>
  <?php endforeach; ?>
</ul>

<script>
$(function() {
    $(".pages").sortable({
      update: function( event, ui ) {
        var $itemMoved = ui.item;
        var id = $itemMoved.attr('data-id');
        var prev = $itemMoved.prev();
        var next = $itemMoved.next();
        if (prev.length > 0 && next.length > 0) {
          $itemMoved.data('order', (prev.data('order') + next.data('order')) / 2);
        } else if (prev.length > 0) {
          $itemMoved.data('order', prev.data('order') + 5);
        } else if (next.length > 0) {
          $itemMoved.data('order', next.data('order') - 5);
        }
        
        $.ajax({
          type: 'POST',
          url: window.location,
          data: {
            'id': id,
            'order': $itemMoved.data('order')
          },
          success: function(data) {
            location.reload();
          }
        });
      }
    });
    $(".pages tbody").disableSelection();
  });
</script>
</div>
</div>