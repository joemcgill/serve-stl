<?php $this->set('title_for_layout', $page['Page']['title']); ?>
<div class="row">
  <h1 class="super"><?php echo $page['Page']['title']; ?></h1>
  <hr />
</div>
<div class="row">
  <?php echo $page['Page']['content']; ?>
</div><!-- .row -->