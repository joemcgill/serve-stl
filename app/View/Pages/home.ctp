<div class="row">
 		 <div class="span1"></div>
         <div class="span6">
         <h1>What is Serve St. Louis?</h1>
         <p>On October 8 &amp; 9, 2016, a movement of churches and over 1,200 Christ-followers throughout the region united in a single weekend of practical acts of service, making the metro area a better place to live, and bringing honor to the name of Jesus.</p>
                  </div><!-- .span4 -->

         <div class="span5">
                 <p class="video-front" style="background:url('/img/serve-gardening.jpg') no-repeat;background-size:400px 225px;width:400px;height:225px;">


                <iframe src="//player.vimeo.com/video/106230357?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="400" height="225" style="border:white solid 2px;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                <!-- <ul id="slider">

  <li><img src="img/serve-painting.jpg" alt="volunteer serving by painting house" /></li>
  <li><img src="img/serve_man_holding_birdhouse.jpg" alt="volunteer smiling while serving" /></li>
  <li><img src="img/serve-gardening.jpg" alt="volunteers serving by gardening" /></li>
  <li><img src="img/serve_cooking.jpg" alt="volunteers cooking" /></li>
  <li><img src="img/serve-painting2.jpg" alt="volunteer serving by painting house" /></li>
  <li><img src="img/serve-family-washing-car.jpg" alt="volunteers serving washing a car" /></li>
  <li><img src="img/serve-painting3.jpg" alt="volunteer serving by painting house" /></li>
    <li><img src="img/serve-measuring.jpg" alt="volunteers serving by building" /></li>
  				</ul>
                -->
                    </p>

                  </div><!-- .span5 -->
 </div><!-- .row -->

	<hr />

	<div class="secondary_content">
		<div id="band">
			<div class="span1"></div>
			<!-- <div class="spanthird"><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'updates'));?>"><img src="<?php echo $this->webroot; ?>img/join_in_icon.png" /></a></div> -->

			<div class="span6 center"><a href="<?php echo $this->Html->url(array('controller' => 'projects', 'action' => 'search'));?>"><img src="<?php echo $this->webroot; ?>img/serve_icon.png" /></a></div>

			<div class="span6 center last"><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'connect'));?>"><img src="<?php echo $this->webroot; ?>img/connect_icon.png" /></a></div>
		</div>


		<div id="description">
			<div class="span1"></div>
			<!-- <div class="spanthird">
				<h2><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'updates'));?>">Updates</a></h2>
				<p class="small">Planning is ramping up for Serve St. Louis 2017! Check back soon for more details.</p>

			</div> -->

			<div class="span6 center">
				<h2><a href="<?php echo $this->Html->url(array('controller' => 'projects', 'action' => 'search'));?>">Projects</a></h2>
				<p class="small">Work projects can be anything from visiting the sick and elderly, to cleaning yards, to painting and construction -whatever meets a practical need in your community.</p></div>

				<div class="span6 center last">
					<h2><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'connect'));?>">Connect</a></h2>
				<p class="small">Serve St Louis is supported by Christian churches, organizations, and
companies representing diverse locations, sizes, and denominations. See
who is participating and make new connections.</p>
				</div>
			</div><!--end description-->



		<div id="button-row">

			<!-- <div class="spanthird">
				<p class="midbutton"><?php echo $this->Html->link('learn more', array('controller' => 'pages', 'action' => 'show', 'information-meetings')); ?></p>
			</div> -->

			<div class="span6 center">
			<p class="midbutton"><?php echo $this->Html->link('learn more', array('controller' => 'projects', 'action' => 'search')); ?></p>
			</div>

			<div class="span6 center last">
			<p class="midbutton"><?php echo $this->Html->link('learn more', array('controller' => 'pages', 'action' => 'show', 'connect')); ?></p>
			</div>
		</div>	 <!--end button-row-->
		</div> <!--end secondary_content-->


	<div id="tertiary_content">
		<div class="row">

			<div class="span6 border_right">
				<h3 class="center">How To</h3>
				<p class="center">How to involve your church or organization</p>
				<iframe width="420" height="236" src="https://www.youtube.com/embed/F89OTLi6VyU" frameborder="0" allowfullscreen></iframe>

				<p class="center"><strong style="color:#fff;">See also: <a href="https://www.youtube.com/watch?v=kJ6MPBr7hWM" target="_blank">how to host a project.</a><strong></p>
			</div>

			<div class="span6">
				<h3 class="center">Current Media Partners</h3>
				<p class="center">We are so excited to have <a href="http://joyfmonline.org/" target="_blank">99.1 JoyFM</a> and <a href="http://boost1019.com/" target="_blank">BOOST 101.9</a> as our media partners.</p>

				<p class="center"><a href="http://joyfmonline.org/" target="_blank">
				<img src="img/joy_fm_logo.png" alt="JoyFM is a current media partner" title="Visit JoyFM Online" /></a>
				<a href="http://boost1019.com/" target="_blank">
				<img src="img/boost_logo.png" alt="BOOST 101.9 is a current media partner" title="Visit BOOST 101.9 Online" /></a></p>
			</div>
		</div> <!--end mid_row-->

	</div> <!--end tertiary_content-->

	<!-- Required -->
<script type="text/javascript">
$(function() {
 $('#slider').anythingSlider(); // add any non-default options here
});
</script>
