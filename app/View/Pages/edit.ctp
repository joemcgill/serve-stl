<div class="row">
  <h1 class="super">Add/Edit Page</h1>
  <hr>
</div>

<?php echo $this->Form->create('Page'); ?>
<?php
echo $this->Form->input('title');
echo $this->Form->input('slug');
echo $this->Form->input('is_active');
echo $this->Form->input('content', array('type' => 'textarea', 'rows' => '25', 'style' => 'width: 100%; height:400px;'));
echo '<br />';
echo $this->Form->end(__('Submit'));
?>

<br /><br />
<h4>Preview:</h4>
<div id="output">
  <p>Nothing to preview...</p>
</div>
<div class="clear"></div>

<?php echo $this->Html->script('tiny_mce/tiny_mce'); ?>
<script>
var editor, 
  $out;

function onchange(e) {
  $out.html(editor.getBody().innerHTML);
  return true;
}
function oninit(instance) {
  editor = instance;
  $out = $("#output");
  onchange({});
}

tinyMCE.init({
  mode: 'textareas',
  theme: 'advanced',
  plugins: 'spellchecker,advimage',
  convert_urls: false,
  handle_event_callback: 'onchange',
  init_instance_callback: 'oninit'
});
</script>