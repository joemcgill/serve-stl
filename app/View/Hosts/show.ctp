<div class="row">
<div class="span2"></div>
<div class="span11">
<?php $this->set('title_for_layout', $host['Host']['name']); ?>
<h1><?php echo $host['Host']['name'] ?></h1>
<hr />
<div class="host_page_image">
   <?php if (isset($host['Host']['image_path']))  {
            echo '<img class="host_image" width="500" height="250" src="' . $this->webroot . $host['Host']['image_path'] . '" />';
          }
          ?>
</div>
<br />
<h2>Projects</h2>

<?php foreach ($host['Project'] as $project): ?>
  <li class="project_list"><?php echo $project['title']; ?></li>
<?php endforeach; ?>
</div>
</div>