<div class="row">
  <h1 class="super">Add/Edit Host</h1>
  <hr>
</div>

<div class="row">
<div class="span12">
<?php
echo $this->Form->create("Host", array('type' => 'file'));

echo $this->Form->input('name');

echo '<br />';

echo $this->Form->input('is_anchor');
echo $this->Form->input('parent_id', array(
  'label' => 'Parent Anchor',
  'empty' => 'Select an anchor',
));

echo '<br />';

echo $this->Form->input('user_id', array('label' => 'Liaison', 'empty' => 'Select a liaison'));

echo $this->Form->input('contact_name');
echo $this->Form->input('contact_email');
echo $this->Form->input('contact_phone_number');
echo $this->Form->input('website_url');

echo '<hr />';
?>

<div class="input class">
  <label for="HostImage">Image (recommended size/ratio 500w x 250h (2:1) or larger)</label>
  <?php
  if (isset($this->data['Host']['image_path'])) {
    echo '<img src="' . $this->webroot . $this->data['Host']['image_path'] . '" /><br />';
  }
  echo $this->Form->file('image');
  ?>
</div>

<?php
echo '<hr />';

echo '<h2>Address Details</h2>';
echo $this->Form->input('address_line_1');
echo $this->Form->input('address_line_2');
echo $this->Form->input('city');
echo $this->Form->input('state');
echo $this->Form->input('zipcode');

echo $this->Form->end(__('Submit'));
?>
</div>
</div>

<script>
var isHostCheckbox = $("#HostIsAnchor");
var parentIdSelect = $("#HostParentId");
var parentIdDiv = parentIdSelect.parent();

function updateParentDiv() {
  if (isHostCheckbox.is(':checked')) {
    parentIdDiv.hide();
    parentIdSelect.prop('selectedIndex',0);
  } else {
    parentIdDiv.show();
  }
}

isHostCheckbox.change(function() {
  updateParentDiv();
});
updateParentDiv();
</script>