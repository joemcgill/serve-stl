<div class="row">
  <h1 class="super">Manage Hosts</h1>
  <hr />
</div>
<div class="row">
<div class="span12">
<p><?php echo $this->Html->link('+ Add new host', array('controller' => 'hosts', 'action' => 'edit')); ?></p>
<?php echo $this->Session->flash(); ?>
<table class="table table-striped">
  <tbody>
  	<thead>
  		<tr>
  			<th>Host Name</th>
  			<th>Manage</th>
  		</tr>
  	</thead>
  
  <?php foreach ($hosts as $host): ?>
    <tr>
      <td><?php echo $this->Html->link($host['Host']['name'], array('action' => 'show', $host['Host']['id'])) ?></td>
      <td>
        <?php echo $this->Html->link(
          'Edit',
          array('action' => 'edit', $host['Host']['id']),
          array('class' => 'btn')
        ); ?>
        &nbsp;&nbsp;
        <?php echo $this->Form->postLink(
          'Delete', 
          array('action' => 'delete', $host['Host']['id']),
          array('confirm' => 'Are you sure?', 'class' => 'btn btn-danger')
        ); ?>
      </td>
    </tr> 
  <?php endforeach; ?>
  </tbody>
</table>
</div>
</div>
