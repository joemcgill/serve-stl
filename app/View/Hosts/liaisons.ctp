<div class="row">
  <h1 class="super">Hosts under <?php echo $host['Host']['name']; ?></h1>
  <hr>
</div>
<div class="row">
  <div class="span12">
  <p><a id="email-members" href="#">Email all liaisons</a></p>
  <?php echo $this->Session->flash(); ?>
  <table class="table table-striped">
    <tbody>
    	<thead>
    		<tr>
    			<th>Host</th>
    			<th>Liaison</th>
    			<th>Email</th>          
    		</tr>
    	</thead>
    <?php foreach ($children as $child_host): ?>
      <tr>
        <td><?php echo $this->Html->link($child_host['Host']['name'], array('action' => 'show', $child_host['Host']['id'])) ?></td>
        <?php $email = $child_host['liaison']['email']; ?>
        <td><?php echo $child_host['liaison']['name']; ?></td>
        <td><a class="member-email" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
      </tr> 
    <?php endforeach; ?>
    </tbody>
  </table>
  </div>
</div>

<script>
$("#email-members").click(function(e) {
  e.preventDefault();

  var mailto = "mailto:";
  $(".member-email").each(function() {
    mailto = mailto + ($(this).text() + ",");
  });
  window.location = mailto;
});
</script>