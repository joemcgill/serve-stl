<?php $this->set('title_for_layout', 'Manage Projects'); ?>
<div class="row">
  <h1 class="super">Manage Projects</h1>
  <hr />
</div>
<div class="row">
<div class="span12">
<p><?php echo $this->Html->link('+ Add new project', array('controller' => 'projects', 'action' => 'edit')); ?></p>
<?php echo $this->Session->flash(); ?>
<table class="table table-striped">
  <tbody>
  	<thead>
  		<tr>
  			<th>Project Name</th>
  			<th>Manage</th>
  		</tr>
  	</thead>
  <?php foreach ($host['Project'] as $project): ?>
    <tr>
      <td><?php echo $this->Html->link($project['title'], array('controller' => 'projects', 'action' => 'show', $project['id'])) ?></td>
      <td>
        <?php echo $this->Html->link(
          'Edit',
          array('controller' => 'projects', 'action' => 'edit', $project['id']),
          array('class' => 'btn')
        ); ?>
        &nbsp;&nbsp;
        <?php echo $this->Form->postLink(
          'Delete', 
          array('controller' => 'projects', 'action' => 'delete', $project['id']),
          array('confirm' => 'Are you sure?', 'class' => 'btn btn-danger')
        ); ?>
      </td>
    </tr> 
  <?php endforeach; ?>
  </tbody>
</table>
</div>
</div>