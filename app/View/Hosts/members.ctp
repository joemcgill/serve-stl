<div class="row">
  <h1 class="super">Members of <?php echo $host['Host']['name']; ?></h1>
  <hr>
</div>
<div class="row">
  <div class="span12">
  <p><a id="email-members" href="#">Email all members</a></p>
  <?php echo $this->Session->flash(); ?>
  <table class="table table-striped">
    <tbody>
    	<thead>
    		<tr>
    			<th>Name</th>
    			<th>Email</th>
    		</tr>
    	</thead>
    <?php foreach ($host['User'] as $member): ?>
      <tr>
        <td><?php echo $member['name']; ?></td>
        <td><a href="mailto:<?php echo $member['email']; ?>" class="member-email"><?php echo $member['email']; ?></a></td>
      </tr> 
    <?php endforeach; ?>
    </tbody>
  </table>
  </div>
</div>

<script>
$("#email-members").click(function(e) {
  e.preventDefault();

  var mailto = "mailto:";
  $(".member-email").each(function() {
    mailto = mailto + ($(this).text() + ",");
  });
  window.location = mailto;
});
</script>