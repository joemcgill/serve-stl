<?php $this->set('title_for_layout', 'Sign up'); ?>
<div class="row">
  <h1 class="super">Sign Up</h1>
  <hr>
</div>
<div class="users signup form">
<?php
  echo $this->Form->create('User');
  echo $this->Form->input('user_name');
  echo $this->Form->input('first_name');
  echo $this->Form->input('last_name');
  echo $this->Form->input('host_id', array(
    'label' => 'Member of (optional)',
    'empty' => 'Select home organization'
  ));
  echo $this->form->input('role', array(
    'type'        => 'checkbox',
    'label'       => 'I\'m a project leader.',
    'value'       => 'project_leader',
    'hiddenField' => false
  ));
  echo '<hr />';
  echo '<h2>Contact Info</h2>';
  echo $this->Form->input('email');
  echo $this->Form->input('phone_number');

  // echo '<hr />';
  // echo '<h2>Address Details</h2>';
  // echo $this->Form->input('address_line_1');
  // echo $this->Form->input('address_line_2');
  // echo $this->Form->input('city');
  // echo $this->Form->input('state');
  // echo $this->Form->input('zipcode');

  echo '<hr />';
  echo '<h2>Where did you hear about Serve St. Louis?</h2>';
  echo $this->Form->select('source',
    array("my_church" => "My Church", "joy_fm" => "Joy FM", "other_media" => "Other Media", "word_of_mouth" => "Word of Mouth", "other" => "Not listed"),
    array('empty' => 'Choose a source'));
  echo $this->Form->input('other_source_text', array(
      'label' => "If not listed, please specify:",
    ));

  echo '<hr />';
  echo $this->Form->input('password');
  echo $this->Form->input('confirm_password', array('type' => 'password'));
  echo $this->Form->end(__('Submit'));
?>
</div>

<script>
var referralSourceSelect = $("#UserSource");
var otherSourceText = $("#UserOtherSourceText");
var parentIdDiv = otherSourceText.parent();

function updateParentDiv() {
  if (referralSourceSelect.val() != 'other') {
    parentIdDiv.hide();
    otherSourceText.val('');
  } else {
    parentIdDiv.show();
  }
}

referralSourceSelect.change(function() {
  updateParentDiv();
});
updateParentDiv();
</script>
