<?php $this->set('title_for_layout', 'Your Dashboard'); ?>
<div class="row">
  <h1 class="super">Your Dashboard</h1>
  <hr />
</div>
<div class="row">
<div class="span12">
<?php $user = $this->Session->read('Auth.User'); ?>
<?php if ($user['role'] === 'admin'): ?>
  <h2>Admin functions</h2>
  <ul class="dashboard">

    <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'manage'));?>"><img src="<?php echo $this->webroot; ?>img/manage_site.png" /><br />Manage Site</a></li>
    <li><a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'manage'));?>"><img src="<?php echo $this->webroot; ?>img/manage_users.png" /><br />Manage Users</a></li>
    <li><a href="<?php echo $this->Html->url(array('controller' => 'hosts', 'action' => 'manage')); ?>"><img src="<?php echo $this->webroot; ?>img/manage_hosts.png" /><br />Manage Hosts</a></li>
    <li><a href="<?php echo $this->Html->url(array('controller' => 'projects', 'action' => 'manage')); ?>"><img src="<?php echo $this->webroot; ?>img/manage_projects.png" /><br />Manage Projects</a></li>
    <li><a href="<?php echo $this->Html->url(array('controller' => 'projects', 'action' => 'export')); ?>"><img src="<?php echo $this->webroot; ?>img/export.png" /><br />Build Export</a></li>


  </ul>

  <hr />
<?php endif;?>

<?php if ($user['Host']['id'] !== null): ?>
  <?php if ($user['Host']['is_anchor']): ?>
      <h2>Anchor functions for <?php echo $user['Host']['name']; ?></h2>
      <ul class="dashboard">
        <li><a href="<?php echo $this->Html->url(array('controller' => 'hosts', 'action' => 'liaisons', $user['Host']['id']));?>"><img src="<?php echo $this->webroot; ?>img/manage_users.png" /><br />View/Email Liaisons</a></li>
      </ul>
      <hr />
  <?php endif;?>

  <h2>Liaison functions for <?php echo $user['Host']['name']; ?></h2>
  <ul class="dashboard">
    <li><a href="<?php echo $this->Html->url(array('controller' => 'hosts', 'action' => 'members', $user['Host']['id']));?>"><img src="<?php echo $this->webroot; ?>img/manage_users.png" /><br />View/Email Members</a></li>
    <li><a href="<?php echo $this->Html->url(array('controller' => 'hosts', 'action' => 'projects', $user['Host']['id'])); ?>"><img src="<?php echo $this->webroot; ?>img/manage_projects.png" /><br />Manage Projects</a></li>

  </ul>
  <hr />
<?php endif; ?>

<?php if ( $user['role'] === 'project_leader' ): ?>
  <h2>Project Leader functions</h2>
  <?php if ( count($projects) > 0 ) : ?>
  <h4>Projects you manage:</h4>
  <ul class="dashboard-list">
    <?php foreach ($projects as $project): ?>
    <li>
      <?php echo $this->Html->link($project['Project']['title'], array('controller' => 'projects', 'action' => 'show', $project['Project']['id'])) ?>
      &nbsp; (<?php echo $this->Html->link(
          'Edit',
          array('controller' => 'projects', 'action' => 'edit', $project['Project']['id']),
          array('class' => 'btn')
        ); ?>)</li>
    <?php endforeach; ?>
  </ul>
  <?php endif; // list projects ?>
  <p>+ <a href="<?php echo $this->Html->url(array('controller' => 'projects', 'action' => 'edit')); ?>">Add a project</a></p>
  <hr />
<?php endif; ?>


<?php
if ( $volunteerSpot !== null ) :
  $project_count = count($volunteerSpot);
  $project_count_text = ( 1 === $project_count ) ? 'one project' : $project_count . ' projects';

?>
  <h2>Manage your projects</h2>
  <p><strong>You're registerd for <?php echo $project_count_text; ?></strong></p>
  <ul class="dashboard-list">
<?php

  foreach( $volunteerSpot as $spot ) {
    $total = $spot['VolunteerSpot']['total_volunteers'];
    $html = $this->Html->link($spot['Project']['title'], array('controller' => 'projects', 'action' => 'show', $spot['Project']['id']));

    if ( $total > 1) {
      $html .= ' with ' . ($total - 1) . ' other volunteers. ';
    }

    $html .= $this->Form->postLink( 'Remove', array('action' => 'volunteer_spot'), array('confirm' => 'Are you sure?', 'class' => 'btn btn-danger') );
    ?>
      <li>You're signed up to volunteer for <?php echo $html; ?></li>
    <?php
  }
?>
  </ul>

    <!--li><?php echo $this->Form->postLink(
    'Give up volunteer spot',
    array('action' => 'volunteer_spot'),
    array('confirm' => 'Are you sure?', 'class' => 'btn btn-danger')
  ); ?></li-->

<h2>Manage your account</h2>
<ul class="dashboard-list">
<?php endif; ?>
  <li><a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'update_password'));?>">Update your password</a></li>
</ul>

</div>
</div><!-- .row -->
