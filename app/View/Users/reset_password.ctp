<?php $this->set('title_for_layout', 'Reset Password'); ?>
<div class="row">
  <h1 class="super">Reset Password</h1>
  <hr>
</div>
<div class="row">
<div class="span 12">
  <p>Forgot your password? No problem! Just enter the email address you registered with Serve St. Louis; we'll generate you a temporary password and email it to you.</p>
  <p>Once you've received your temporary password and have logged in, be sure to change your password to something to your liking via your dashboard.</p>
<div class="users form">
<?php 
echo $this->Form->create('User');
echo $this->Form->input('email');
echo $this->Form->end(__('Reset Password'));
?>
</div>
</div>
</div>