<div class="row">
  <h1 class="super">Manage Users</h1>
  <hr />
</div>
<div class="row">
<div class="span12">
<p><?php echo $this->Html->link('+ Add new user', array('controller' => 'users', 'action' => 'add')); ?></p>
<?php echo $this->Session->flash(); ?>
<a href="#volunteers" class="white btn-lg pull-right">Scroll Down to Volunteers</a>
<h2>Contributors / Admins</h2>
<table class="table table-striped">
  <tbody>
  	<thead>
  		<tr>
  			<th>User</th>
  			<th>Role</th>
  			<th>Manage</th>
  		</tr>
  	</thead>
  <?php foreach ($backend_users as $aUser): ?>
    <tr>
      <td><?php echo $aUser['User']['name']; ?></td>
      <td><?php echo $aUser['User']['role']; ?></td>
      <td>
        <?php echo $this->Html->link(
          'Edit',
          array('action' => 'edit', $aUser['User']['id']),
          array('class' => 'btn')
        ); ?>
        &nbsp;&nbsp;
        <?php echo $this->Form->postLink(
          'Delete',
          array('action' => 'delete', $aUser['User']['id']),
          array('confirm' => 'Are you sure?', 'class' => 'btn btn-danger')
        ); ?>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

<br />
<hr />
<br />
<a name="volunteers"></a>
<h2>Volunteers</h2>
<p class="pull-right"><img src="<?php echo $this->webroot; ?>img/people_icon.png" style="display:inline; position:relative; top:-3px;" /><?php echo $volunteerCount; ?> total volunteers signed up</p>
<table class="table table-striped">
  <tbody>
  	<thead>
  		<tr>
  			<th>User</th>
  			<th>Role</th>
  			<th>Manage</th>
  		</tr>
  	</thead>
  <?php foreach ($volunteers as $aUser): ?>
    <tr>
      <td><?php echo $aUser['User']['name']; ?></td>
      <td><?php echo $aUser['User']['role']; ?></td>
      <td>
        <?php echo $this->Html->link(
          'Edit',
          array('action' => 'edit', $aUser['User']['id']),
          array('class' => 'btn')
        ); ?>
        &nbsp;&nbsp;
        <?php echo $this->Form->postLink(
          'Delete',
          array('action' => 'delete', $aUser['User']['id']),
          array('confirm' => 'Are you sure?', 'class' => 'btn btn-danger')
        ); ?>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

</div>
</div>
