<?php $this->set('title_for_layout', 'Login'); ?>
<div class="row">
  <h1 class="super">Login</h1>
  <hr>
</div>

<div class="row users login form">
  <div class="span7">
    <?php
      echo $this->Session->flash('auth');
      echo $this->Form->create('User');
      echo $this->Form->input('user_name');
      echo $this->Form->input('password');
      echo $this->Form->end(__('Login'));
    ?>
    <p><?php echo $this->Html->link('Register now', array('controller' => 'users', 'action' => 'signup')); ?> if you don't have a login yet.  It's super easy!</p>
    <p><?php echo $this->Html->link('Reset my password', array('controller' => 'users', 'action' => 'reset_password')); ?></p>

  </div>
  <div class = "span4">
    <?php echo $this->Html->image('login_icon_large.png') ?>
  </div>
</div>

<br />
