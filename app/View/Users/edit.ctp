<?php $this->set('title_for_layout', 'Update Account'); ?>
<div class="row">
  <h1 class="super">Update Account</h1>
  <hr>
</div>
<div class="users form">
<?php
echo $this->Form->create('User');
echo $this->Form->input('user_name');
echo $this->Form->input('first_name');
echo $this->Form->input('last_name');

echo $this->Form->input('role', array(
  'options' => array('volunteer' => 'Volunteer', 'project_leader' => 'Project Leader', 'admin' => 'Admin')
));

echo $this->Form->input('host_id', array(
  'label' => 'Member of (optional)',
  'empty' => 'Select home organization'
));

echo '<br />';
echo '<h2>Contact Info</h2>';
echo $this->Form->input('email');
echo $this->Form->input('phone_number');

echo '<br />';
echo '<h2>Address Details</h2>';
echo $this->Form->input('address_line_1');
echo $this->Form->input('address_line_2');
echo $this->Form->input('city');
echo $this->Form->input('state');
echo $this->Form->input('zipcode');

echo '<p>';
echo $this->Html->link('Click to update password', array('controller' => 'users', 'action' => 'update_password', $user_id));
echo '</p>';
echo $this->Form->end(__('Submit'));
?>
</div>
