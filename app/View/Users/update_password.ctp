<?php $this->set('title_for_layout', 'Change Your Password'); ?>
<div class="row">
  <h1 class="super">Change Your Password</h1>
  <hr>
</div>
<div class="users form">
<?php
echo $this->Form->create('User');
echo $this->Form->input('current_password', array('type' => 'password'));
echo '<br />';
echo $this->Form->input('password', array('label' => 'New Password'));
echo $this->Form->input('confirm_password', array('type' => 'password', 'label' => 'Confirm New Password'));  
echo $this->Form->end(__('Submit'));
?>
</div>