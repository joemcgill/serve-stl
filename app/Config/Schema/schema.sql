CREATE TABLE `pages` (
    `id` INT UNSIGNED AUTO_INCREMENT,
    `parent_id` INT UNSIGNED,
    `order` float DEFAULT 0,
    `title` varchar(255) NOT NULL,
    `content` text DEFAULT NULL,
    `slug` varchar(255) NOT NULL,
    `is_active` tinyint(1) DEFAULT 1,
    `created` DATETIME DEFAULT NULL,
    `modified` DATETIME DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `slug` (`slug`)    
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

CREATE TABLE `users` (
    `id` INT UNSIGNED AUTO_INCREMENT,
    `host_id` INT UNSIGNED DEFAULT NULL,
    `first_name` VARCHAR(64),
    `last_name` VARCHAR(64),
    `email` VARCHAR(64),
    `phone_number` varchar(32) DEFAULT NULL,
    `password` VARCHAR(128),
    `role` VARCHAR(20),
    `address_line_1` varchar(255) DEFAULT NULL,
    `address_line_2` varchar(255) DEFAULT NULL,
    `city` varchar(255) DEFAULT NULL,
    `state` varchar(255) DEFAULT NULL,
    `zipcode` varchar(16) DEFAULT NULL,
    `source` varchar(255) DEFAULT NULL,
    `other_source_text` varchar(255) DEFAULT NULL,
    `created` DATETIME DEFAULT NULL,
    `modified` DATETIME DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

CREATE TABLE `subscribers` (
    `id` INT UNSIGNED AUTO_INCREMENT,
    `email` VARCHAR(64),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

CREATE TABLE `hosts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` INT UNSIGNED,
  `user_id` INT UNSIGNED,
  `name` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `contact_name` varchar(128) DEFAULT NULL,
  `contact_email` varchar(128) DEFAULT NULL,  
  `contact_phone_number` varchar(32) DEFAULT NULL,
  `address_line_1` varchar(255) DEFAULT NULL,
  `address_line_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zipcode` varchar(16) DEFAULT NULL,
  `website_url` varchar(255) DEFAULT NULL,
  `is_anchor` tinyint(1) DEFAULT 0,  
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

CREATE TABLE `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `host_id` INT UNSIGNED,
  `user_id` INT UNSIGNED,  
  `title` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `close_date` date DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `minimum_volunteer_age` int(10) unsigned DEFAULT 0,  
  `number_of_volunteers_needed` int(10) unsigned DEFAULT 0,
  `volunteer_count` int(10) unsigned DEFAULT 0,  
  `materials_needed` varchar(512) DEFAULT NULL,
  `special_instructions` text DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,  

  `address_line_1` varchar(255) DEFAULT NULL,
  `address_line_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zipcode` varchar(16) DEFAULT NULL,
  `latitude` varchar(32) DEFAULT NULL,
  `longitude` varchar(32) DEFAULT NULL,  

  `family_friendly` tinyint(1) DEFAULT 0,
  `skilled` tinyint(1) DEFAULT 0,
  `indoor` tinyint(1) DEFAULT 0,
  `outdoor` tinyint(1) DEFAULT 0,
  `physical_labor` tinyint(1) DEFAULT 0,

  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

CREATE TABLE `volunteer_spots` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` INT UNSIGNED,
  `user_id` INT UNSIGNED,
  `message_to_project_leader` varchar(512) DEFAULT NULL,
  `total_volunteers` INT UNSIGNED,
  `family_info` varchar(1024) DEFAULT NULL,
  
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;
